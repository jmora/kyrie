package es.upm.fi.dia.oeg.kyrie.optimizer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import de.normalisiert.utils.graphs.ElementaryCyclesSearch;
import es.upm.fi.dia.oeg.kyrie.rewriter.Clause;
import es.upm.fi.dia.oeg.kyrie.rewriter.Substitution;
import es.upm.fi.dia.oeg.kyrie.rewriter.Term;
import es.upm.fi.dia.oeg.kyrie.rewriter.TermFactory;
import es.upm.fi.dia.oeg.kyrie.rewriter.Variable;

@SuppressWarnings("unchecked")
public class Optimizer {

	private TermFactory m_termFactory;

	public Optimizer(TermFactory termFactory) {
		this.m_termFactory = termFactory;
	}

	public ArrayList<Clause> condensate(ArrayList<Clause> clauses) {

		ArrayList<Clause> result = new ArrayList<Clause>();

		for (Clause c : clauses)
			result.add(this.condensate(c));

		return result;
	}

	public Clause condensate(Clause clause) {

		ArrayList<Clause> unprocessed = new ArrayList<Clause>();
		ArrayList<Clause> condensations = new ArrayList<Clause>();

		unprocessed.add(clause);

		while (!unprocessed.isEmpty()) {
			Clause givenClause = unprocessed.remove(0);
			condensations.add(givenClause);
			for (int i = 0; i < givenClause.getBody().length - 1; i++)
				for (int j = i + 1; j < givenClause.getBody().length; j++) {
					Substitution unifier = Substitution.mostGeneralUnifier(givenClause.getBody()[i], givenClause.getBody()[j], this.m_termFactory);
					if (unifier != null) {
						Clause newQuery = this.getNewQuery(unifier, givenClause);
						boolean isRedundant = false;
						for (Clause unprocessedClause : unprocessed)
							if (unprocessedClause.isEquivalentUpToVariableRenaming(newQuery))
								isRedundant = true;
						if (!isRedundant)
							for (Clause workedOffClause : condensations)
								if (workedOffClause.isEquivalentUpToVariableRenaming(newQuery))
									isRedundant = true;
						if (!isRedundant)
							unprocessed.add(newQuery);
					}
				}
		}
		Collections.sort(condensations, new Comparator<Clause>() {
			public int compare(Clause c1, Clause c2) {
				return new Integer(c1.getBody().length).compareTo(new Integer(c2.getBody().length));
			}
		});
		for (Clause c : condensations)
			if (c.subsumes(clause, false))
				return c;
		return clause;
	}

	private List<List<String>> getCycles(ArrayList<Clause> clauses, Collection<String> mappings, boolean auxs) {
		HashSet<String> predicates = new HashSet<String>();
		for (Clause c : clauses) {
			predicates.add(c.getHead().getName());
			for (Term t : c.getBody())
				predicates.add(t.getName());
		}
		ArrayList<String> nodes;
		if (!auxs) {
			predicates.removeAll(mappings);
			nodes = new ArrayList<String>(predicates);
		} else {
			nodes = new ArrayList<String>();
			for (String p : predicates)
				if (p.contains("AUX$"))
					nodes.add(p);
		}
		boolean adjMatrix[][] = new boolean[nodes.size()][nodes.size()];
		int j, i = 0;
		for (Clause c : clauses) {
			i = nodes.indexOf(c.getHead().getName());
			if (i != -1)
				for (Term t : c.getBody()) {
					j = nodes.indexOf(t.getName());
					if (j != -1)
						adjMatrix[i][j] = true;
				}
		}

		ElementaryCyclesSearch ecs = new ElementaryCyclesSearch(adjMatrix, nodes.toArray());
		return ecs.getElementaryCycles();
	}

	private List<List<String>> getExistentialCycles(ArrayList<Clause> clauses, Collection<String> mappings) {
		HashSet<String> predicates = new HashSet<String>();
		for (Clause c : clauses) {
			predicates.add(c.getHead().getName());
			for (Term t : c.getBody())
				predicates.add(t.getName());
		}
		predicates.removeAll(mappings);
		ArrayList<String> nodes = new ArrayList<String>(predicates);
		boolean adjMatrix[][] = new boolean[nodes.size()][nodes.size()];
		boolean creativeMatrix[][] = new boolean[nodes.size()][nodes.size()];
		int j, i = 0;
		for (Clause c : clauses) {
			i = nodes.indexOf(c.getHead().getName());
			if (i != -1)
				for (Term t : c.getBody()) {
					j = nodes.indexOf(t.getName());
					if (j != -1) {
						adjMatrix[i][j] = true;
						if (c.getBody().length > 1)
							creativeMatrix[i][j] = true;
					}
				}
		}

		ElementaryCyclesSearch ecs = new ElementaryCyclesSearch(adjMatrix, nodes.toArray());
		List<List<String>> res = ecs.getElementaryCycles();
		List<List<String>> rres = new ArrayList<List<String>>();
		for (List<String> cycle : res) {
			int l = cycle.size();
			cycle.add(cycle.get(0));
			for (int k = 0; k < l; k++)
				if (creativeMatrix[nodes.indexOf(cycle.get(k))][nodes.indexOf(cycle.get(k + 1))]) {
					cycle.remove(l);
					rres.add(cycle);
					break;
				}
		}
		return rres;
	}

	private ArrayList<String> getDirectReachablePredicates(String p, ArrayList<Clause> clauses) {
		ArrayList<String> result = new ArrayList<String>();
		// if the head is reachable then all predicates in the body are
		// reachable too
		for (Clause c : clauses)
			if (p.equals(c.getHead().getName()))
				for (Term t : c.getBody())
					result.add(t.getName());
		return result;
	}

	private Clause getNewQuery(Substitution mgu, Clause clause) {

		Set<Term> newBody = new LinkedHashSet<Term>();

		// Copy the atoms from the main premise
		for (int index = 0; index < clause.getBody().length; index++)
			newBody.add(clause.getBody()[index].apply(mgu, this.m_termFactory));

		// New body and head
		Term[] body = new Term[newBody.size()];
		newBody.toArray(body);
		Term head = clause.getHead().apply(mgu, this.m_termFactory);

		Clause newQuery = new Clause(body, head);

		// Rename variables in new query
		ArrayList<Variable> variablesNewQuery = newQuery.getVariables();
		HashMap<Variable, Integer> variableMapping = new HashMap<Variable, Integer>();

		for (int i = 0; i < variablesNewQuery.size(); i++)
			variableMapping.put(variablesNewQuery.get(i), i);

		Clause newQueryRenamed = newQuery.renameVariables(this.m_termFactory, variableMapping);

		return newQueryRenamed;
	}

	private ArrayList<String> getStopPoints(List<List<String>> cycles) {
		ArrayList<String> stopPoints = new ArrayList<String>();
		HashMap<String, Integer> counts;
		ArrayList<List<String>> newcycles;
		int j, i = 0;

		while (cycles.size() > 0) {
			counts = new HashMap<String, Integer>();
			for (List<String> l : cycles)
				for (String s : l) {
					i = counts.containsKey(s) ? counts.get(s) : 0;
					counts.put(s, i + 1);
				}
			String candidate = null;
			for (String k : counts.keySet()) {
				j = counts.get(k);
				if (j > i) {
					i = j;
					candidate = k;
				} else if (j == i)
					if (!k.startsWith("AUX$") || candidate == null || candidate.startsWith("AUX$"))
						candidate = k;
			}
			stopPoints.add(candidate);
			newcycles = new ArrayList<List<String>>();
			for (List<String> l : cycles)
				if (!l.contains(candidate))
					newcycles.add(l);
			cycles = newcycles;
		}

		return stopPoints;
	}

	private ArrayList<Clause> innerPruneWithDependencyGraph(ArrayList<String> preds, ArrayList<Clause> clauses) {
		ArrayList<Clause> result = new ArrayList<Clause>();
		ArrayList<String> predicatesToProcess = new ArrayList<String>(preds);
		HashSet<String> reachablePredicates = new HashSet<String>();
		while (predicatesToProcess.size() > 0) {
			String p = predicatesToProcess.get(0);
			predicatesToProcess.remove(p);

			if (!reachablePredicates.contains(p)) {
				reachablePredicates.add(p);
				predicatesToProcess.addAll(this.getDirectReachablePredicates(p, clauses));
			}
		}
		// Keep clauses with reachable heads only
		for (Clause c : clauses)
			if (reachablePredicates.contains(c.getHead().getName()))
				result.add(c);
		return result;
	}

	public ArrayList<Clause> pruneWithDependencyGraph(ArrayList<Clause> query, ArrayList<Clause> clauses) {
		ArrayList<String> preds = new ArrayList<String>();
		for (Clause c : query) {
			preds.add(c.getHead().getName());
			for (Term t : c.getBody())
				preds.add(t.getName());
		}
		return this.innerPruneWithDependencyGraph(preds, clauses);
	}

	/**
	 * Prunes a set of clauses based on the dependency graph from a specific predicate
	 * 
	 * @return
	 */
	public ArrayList<Clause> pruneWithDependencyGraph(String pred, ArrayList<Clause> clauses) {
		ArrayList<String> preds = new ArrayList<String>();
		preds.add(pred);
		return this.innerPruneWithDependencyGraph(preds, clauses);
	}

	public ArrayList<Clause> pruneWithDependencyGraphAndMappings(ArrayList<Clause> query, ArrayList<Clause> clauses, Collection<String> mappings, Boolean stop) {
		ArrayList<String> preds = new ArrayList<String>();
		ArrayList<String> rea = new ArrayList<String>();
		for (Clause c : query) {
			preds.add(c.getHead().getName());
			rea.add(c.getHead().getName());
			for (Term t : c.getBody())
				preds.add(t.getName());
		}
		return this.pruneWithDependencyGraphAndMappings(stop, preds, rea, clauses, mappings);
	}

	/**
	 * Prunes a set of clauses based on the dependency graph from a specific predicate Very similar to previous method "pruneWithDependencyGraph", may need factorization ToDo.
	 * 
	 * @return
	 */
	public ArrayList<Clause> pruneWithDependencyGraphAndMappings(String pred, ArrayList<Clause> clauses, Collection<String> mappings, Boolean stop) {
		ArrayList<String> preds = new ArrayList<String>();
		ArrayList<String> rea = new ArrayList<String>();
		preds.add(pred);
		rea.add(pred);
		for (Clause c : clauses) {
			c.getHead().getName().equals(pred);
			for (Term t : c.getBody())
				preds.add(t.getName());
		}
		return this.pruneWithDependencyGraphAndMappings(stop, preds, rea, clauses, mappings);
	}

	private ArrayList<Clause> pruneWithDependencyGraphAndMappings(Boolean stop, ArrayList<String> predsToProc, ArrayList<String> predsReachable, ArrayList<Clause> clauses, Collection<String> mappings) {
		ArrayList<Clause> result = new ArrayList<Clause>();
		Set<String> reachablePredicates = new HashSet<String>(predsReachable);
		Set<String> instantiable = new HashSet<String>(mappings);
		boolean instantiableOne;

		if (!stop) {
			int rps = 0; // reachable previous size
			int ips = 0; // instantiable previous size
			while (reachablePredicates.size() != rps || instantiable.size() != ips) {
				rps = reachablePredicates.size();
				ips = instantiable.size();
				for (Clause c : clauses) {
					if (reachablePredicates.contains(c.getHead().getName()))
						for (Term t : c.getBody())
							reachablePredicates.add(t.getName());
					instantiableOne = true;
					for (Term t : c.getBody())
						instantiableOne &= instantiable.contains(t.getName());
					if (instantiableOne)
						instantiable.add(c.getHead().getName());
				}
			}
			for (Clause c : clauses) {
				instantiableOne = reachablePredicates.contains(c.getHead().getName());
				for (Term t : c.getBody())
					instantiableOne &= instantiable.contains(t.getName());
				if (instantiableOne)
					result.add(c);
			}
		} else {
			ArrayList<String> predicatesToProcess = new ArrayList<String>(predsToProc);
			while (predicatesToProcess.size() > 0) {
				String p = predicatesToProcess.get(0);
				predicatesToProcess.remove(p);
				if (!reachablePredicates.contains(p) && !mappings.contains(p)) {
					reachablePredicates.add(p);
					predicatesToProcess.addAll(this.getDirectReachablePredicates(p, clauses));
				}
			}
			for (Clause c : clauses)
				if (reachablePredicates.contains(c.getHead().getName()))
					result.add(c);
			return result;
		}

		return result;
	}

	/**
	 * Performs a query subsumption test
	 * 
	 * @return
	 */
	public ArrayList<Clause> querySubsumptionCheck(ArrayList<Clause> clauses) {
		ArrayList<Clause> result = new ArrayList<Clause>();

		boolean c1IsSubsumed;

		// Identify equivalent clauses
		for (Clause c1 : clauses)
			for (Clause c2 : clauses)
				if (!c1.equals(c2))
					if (c2.subsumes(c1, false) && c1.subsumes(c2, false))
						if (c1.m_canonicalRepresentation.length() < c2.m_canonicalRepresentation.length())
							c2.toBeIgnored = true;
						else
							c1.toBeIgnored = true;

		// Prune subsumed clauses
		for (Clause c1 : clauses)
			if (!c1.toBeIgnored) {
				c1IsSubsumed = false;
				for (Clause c2 : clauses)
					if (!c2.toBeIgnored)
						if (!c1.equals(c2))
							if (c2.subsumes(c1, false)) {
								c1IsSubsumed = true;
								break;
							}
				if (!c1IsSubsumed)
					result.add(c1);
			}

		return result;
	}

	public Collection<String> stopAuxPoints(ArrayList<Clause> clauses) {
		return this.getStopPoints(this.getCycles(clauses, null, true));
	}

	public Collection<String> stopPoints(ArrayList<Clause> clauses, Collection<String> mappings) {
		return this.getStopPoints(this.getCycles(clauses, mappings, false));
	}

	public Collection<String> stopExistentialPoints(ArrayList<Clause> clauses, Collection<String> mappings) {
		return this.getStopPoints(this.getExistentialCycles(clauses, mappings));
	}

	public Collection<String> stopExistentialPoints(ArrayList<Clause> clauses) {
		return this.getStopPoints(this.getExistentialCycles(clauses, new ArrayList<String>()));
	}

}
