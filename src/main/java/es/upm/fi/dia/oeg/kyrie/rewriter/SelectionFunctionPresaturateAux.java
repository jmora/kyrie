// Copyright 2008 by Oxford University; see license.txt for details

package es.upm.fi.dia.oeg.kyrie.rewriter;

import java.util.Collection;


public class SelectionFunctionPresaturateAux extends SelectionFunction {

	private Collection<String> stopPoints;

	/**
	 * @param stopPoints
	 */
	public SelectionFunctionPresaturateAux(Collection<String> stopPoints) {
		this.stopPoints = stopPoints;
	}

	@Override
	public void selectAtoms(Clause c) {

		// Initialize
		c.m_selectedHead = c.getHead().getName().contains("AUX$") && !this.stopPoints.contains(c.getHead().getName());
		c.m_selectedBody = new boolean[c.getBody().length];
		for (int i = 0; i < c.getBody().length; i++)
			if (c.getBody()[i].getName().contains("AUX$") && !this.stopPoints.contains(c.getBody()[i].getName())) {
				c.m_selectedBody[i] = true;
				c.m_selectedHead = false;
			}
	}

	@Override
	public boolean isToBePruned(Clause c) {
		for (Term t : c.getBody())
			if (t.getName().contains("AUX$") && !this.stopPoints.contains(t.getName()))
				return true;
		return c.getHead().getName().contains("AUX$") && !this.stopPoints.contains(c.getHead().getName());
	}

}
