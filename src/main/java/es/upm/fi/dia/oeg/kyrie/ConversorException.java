package es.upm.fi.dia.oeg.kyrie;

/**
 * 
 */

/**
 * @author jmora
 * 
 */
public class ConversorException extends Exception {

	private static final long serialVersionUID = -2207139018767157103L;

	public ConversorException() {/* ignore */
	}

	public ConversorException(String arg0) {
		super(arg0);
	}

	public ConversorException(Throwable arg0) {
		super(arg0);
	}

	public ConversorException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
