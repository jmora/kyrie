package es.upm.fi.dia.oeg.kyrie.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import es.upm.fi.dia.oeg.kyrie.rewriter.Clause;


public class RView {

	private static final RModel m_model = new RModel(); // @jve:decl-index=0:
	String ontologyPath = ""; // @jve:decl-index=0:
	String mappingsPath = ""; // @jve:decl-index=0:
	String query = ""; // @jve:decl-index=0:
	String[] databaseConnectionData = new String[] { "", "", "", "" };
	ArrayList<Clause> rewriting = null;

	private JFrame jFrame = null;
	private JDialog aboutDialog = null;
	private JPanel aboutContentPane = null;
	private JLabel aboutVersionLabel = null;
	private JPanel ontologyPanel = null;
	private JPanel jPanel5 = null;
	private JButton jButton = null;
	private JPanel jPanel = null;
	private JLabel jLabel10 = null;
	private JTextField databaseURL = null;
	private JLabel jLabel11 = null;
	private JTextField databaseName = null;
	private JLabel jLabel12 = null;
	private JTextField username = null;
	private JLabel jLabel13 = null;
	private JPasswordField password = null;
	private JPanel DBPanel = null;
	private JPanel queryPanel = null;
	private JScrollPane jScrollPane1 = null;
	private JPanel rewritingPanel = null;
	private JScrollPane jScrollPane2 = null;
	private JTextArea rewritingTextArea = null;
	private JPanel jPanel1 = null;
	private JButton jButton3 = null;
	private JButton jButton4 = null;
	private JButton jButton5 = null;
	private JFileChooser jFileChooser = null;
	private JTextField ontologyTextField = null;
	private JTextArea queryTextArea = null;
	private JOptionPane jOptionPane = null;
	private JPanel jPanel9 = null;
	private JPanel jPanel10 = null;
	private JMenu jMenu = null;
	private JMenuItem jMenuItem = null;
	private JMenuItem jMenuItem1 = null;
	private JMenuBar jJMenuBar = null;
	private JMenu jMenu1 = null;
	private JMenuItem jMenuItem2 = null;
	private JMenuItem jMenuItem3 = null;
	private JMenu jMenu2 = null;
	private JMenuItem jMenuItem4 = null;
	private JMenuItem jMenuItem7 = null;
	private JPanel SQLPanel = null;
	private JScrollPane jScrollPane21 = null;
	private JTextArea SQLTextArea = null;
	private JSplitPane jSplitPane = null;
	private JSplitPane jSplitPane1 = null;
	private JPanel answerPanel = null;
	private JScrollPane jScrollPane22 = null;
	private JTextArea answerTextArea = null;
	private JMenu jMenu4 = null;
	private JMenuItem jMenuItem8 = null;
	private JSeparator jSeparator = null;
	private JPanel mappingsPanel = null;
	private JPanel jPanel51 = null;
	private JButton jButton1 = null;
	private JTextField mappingsTextField = null;
	private JPanel jPanel6 = null;
	private JPanel jPanel7 = null;

	/**
	 * This method initializes jFrame
	 * 
	 * @return javax.swing.JFrame
	 */
	private JFrame getJFrame() {
		if (this.jFrame == null) {
			this.jFrame = new JFrame();
			this.jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.jFrame.setSize(1000, 700);
			this.jFrame.setContentPane(this.getJPanel9());
			this.jFrame.setJMenuBar(this.getJJMenuBar());
		}
		return this.jFrame;
	}

	/**
	 * This method initializes aboutDialog
	 * 
	 * @return javax.swing.JDialog
	 */
	private JDialog getAboutDialog() {
		if (this.aboutDialog == null) {
			this.aboutDialog = new JDialog(this.getJFrame(), true);
			this.aboutDialog.setTitle("About");
			this.aboutDialog.setContentPane(this.getAboutContentPane());
		}
		return this.aboutDialog;
	}

	/**
	 * This method initializes aboutContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getAboutContentPane() {
		if (this.aboutContentPane == null) {
			this.aboutContentPane = new JPanel();
			this.aboutContentPane.setLayout(new BorderLayout());
			this.aboutContentPane.add(this.getAboutVersionLabel(), BorderLayout.CENTER);
		}
		return this.aboutContentPane;
	}

	/**
	 * This method initializes aboutVersionLabel
	 * 
	 * @return javax.swing.JLabel
	 */
	private JLabel getAboutVersionLabel() {
		if (this.aboutVersionLabel == null) {
			this.aboutVersionLabel = new JLabel();
			this.aboutVersionLabel.setText("Version 1.0");
			this.aboutVersionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return this.aboutVersionLabel;
	}

	/**
	 * This method initializes ontologyPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getOntologyPanel() {
		if (this.ontologyPanel == null) {
			this.ontologyPanel = new JPanel();
			this.ontologyPanel.setLayout(new BorderLayout());
			this.ontologyPanel.setName("");
			this.ontologyPanel.setBorder(BorderFactory.createTitledBorder(null, "Ontology", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
					null, null));
			this.ontologyPanel.setPreferredSize(new Dimension(820, 57));
			this.ontologyPanel.add(this.getJPanel5(), BorderLayout.EAST);
			this.ontologyPanel.add(this.getOntologyTextField(), BorderLayout.CENTER);
		}
		return this.ontologyPanel;
	}

	/**
	 * This method initializes jPanel5
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel5() {
		if (this.jPanel5 == null) {
			this.jPanel5 = new JPanel();
			this.jPanel5.setLayout(new BorderLayout());
			this.jPanel5.add(this.getJButton(), BorderLayout.EAST);
		}
		return this.jPanel5;
	}

	/**
	 * This method initializes jButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getJButton() {
		if (this.jButton == null) {
			this.jButton = new JButton();
			this.jButton.setText("Choose...");
			this.jButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					RView.this.jFileChooser = null;
					RView.this.jFileChooser = RView.this.getJFileChooser();

					ExtensionFileFilter filter = new ExtensionFileFilter();
					filter.addExtension("owl", false);
					filter.setDescription("OWL files");
					RView.this.jFileChooser.setFileFilter(filter);

					int value = RView.this.jFileChooser.showOpenDialog(RView.this.jFrame);

					if (value == JFileChooser.APPROVE_OPTION) {
						RView.this.ontologyPath = RView.this.jFileChooser.getSelectedFile().getAbsolutePath();
						RView.this.ontologyTextField.setText(RView.this.ontologyPath);
					}
				}
			});
		}
		return this.jButton;
	}

	/**
	 * This method initializes jPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel() {
		if (this.jPanel == null) {
			this.jLabel13 = new JLabel();
			this.jLabel13.setText("\tPassword:");
			this.jLabel13.setPreferredSize(new Dimension(51, 16));
			this.jLabel12 = new JLabel();
			this.jLabel12.setText("\tUsername:");
			this.jLabel12.setPreferredSize(new Dimension(51, 16));
			this.jLabel11 = new JLabel();
			this.jLabel11.setText("\tDatabase name:");
			this.jLabel11.setPreferredSize(new Dimension(51, 16));
			this.jLabel10 = new JLabel();
			this.jLabel10.setText("\tDBMS URL:");
			this.jLabel10.setPreferredSize(new Dimension(51, 16));
			GridLayout gridLayout1 = new GridLayout();
			gridLayout1.setRows(4);
			gridLayout1.setColumns(2);
			this.jPanel = new JPanel();
			this.jPanel.setLayout(gridLayout1);
			this.jPanel.add(this.jLabel10, null);
			this.jPanel.add(this.getDatabaseURL(), null);
			this.jPanel.add(this.jLabel11, null);
			this.jPanel.add(this.getDatabaseName(), null);
			this.jPanel.add(this.jLabel12, null);
			this.jPanel.add(this.getUsername(), null);
			this.jPanel.add(this.jLabel13, null);
			this.jPanel.add(this.getPassword(), null);
		}
		return this.jPanel;
	}

	/**
	 * This method initializes databaseURL
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getDatabaseURL() {
		if (this.databaseURL == null) {
			this.databaseURL = new JTextField();
			this.databaseURL.setText("jdbc:postgresql://localhost:5432/");
		}
		return this.databaseURL;
	}

	/**
	 * This method initializes databaseName
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getDatabaseName() {
		if (this.databaseName == null) {
			this.databaseName = new JTextField();
			this.databaseName.setText("wn_jpn_postgresql");
		}
		return this.databaseName;
	}

	/**
	 * This method initializes username
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getUsername() {
		if (this.username == null) {
			this.username = new JTextField();
			this.username.setText("postgres");
		}
		return this.username;
	}

	/**
	 * This method initializes password
	 * 
	 * @return javax.swing.JPasswordField
	 */
	private JPasswordField getPassword() {
		if (this.password == null) {
			this.password = new JPasswordField();
			this.password.setText("820827");
		}
		return this.password;
	}

	/**
	 * This method initializes DBPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getDBPanel() {
		if (this.DBPanel == null) {
			this.DBPanel = new JPanel();
			this.DBPanel.setLayout(new BorderLayout());
			this.DBPanel.setBorder(BorderFactory.createTitledBorder(null, "Database Settings", TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, null, null));
			this.DBPanel.setPreferredSize(new Dimension(274, 140));
			this.DBPanel.add(this.getJPanel(), BorderLayout.NORTH);
		}
		return this.DBPanel;
	}

	/**
	 * This method initializes queryPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getQueryPanel() {
		if (this.queryPanel == null) {
			this.queryPanel = new JPanel();
			this.queryPanel.setLayout(new BorderLayout());
			this.queryPanel.setBorder(BorderFactory.createTitledBorder(null, "Query", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null,
					null));
			this.queryPanel.add(this.getJScrollPane1(), BorderLayout.CENTER);
		}
		return this.queryPanel;
	}

	/**
	 * This method initializes jScrollPane1
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane1() {
		if (this.jScrollPane1 == null) {
			this.jScrollPane1 = new JScrollPane();
			this.jScrollPane1.setPreferredSize(new Dimension(145, 100));
			this.jScrollPane1.setViewportView(this.getQueryTextArea());
		}
		return this.jScrollPane1;
	}

	/**
	 * This method initializes rewritingPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getRewritingPanel() {
		if (this.rewritingPanel == null) {
			this.rewritingPanel = new JPanel();
			this.rewritingPanel.setLayout(new BorderLayout());
			this.rewritingPanel.setBorder(BorderFactory.createTitledBorder(null, "Rewriting", TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, new Font("Lucida Grande", Font.PLAIN, 13), Color.black));
			this.rewritingPanel.add(this.getJScrollPane2(), BorderLayout.CENTER);
		}
		return this.rewritingPanel;
	}

	/**
	 * This method initializes jScrollPane2
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane2() {
		if (this.jScrollPane2 == null) {
			this.jScrollPane2 = new JScrollPane();
			this.jScrollPane2.setViewportView(this.getRewritingTextArea());
		}
		return this.jScrollPane2;
	}

	/**
	 * This method initializes rewritingTextArea
	 * 
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getRewritingTextArea() {
		if (this.rewritingTextArea == null) {
			this.rewritingTextArea = new JTextArea();
			this.rewritingTextArea.setEditable(false);
			this.rewritingTextArea.setEnabled(true);
		}
		return this.rewritingTextArea;
	}

	/**
	 * This method initializes jPanel1
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel1() {
		if (this.jPanel1 == null) {
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 2;
			gridBagConstraints1.gridy = 0;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 1;
			gridBagConstraints.gridy = 0;
			this.jPanel1 = new JPanel();
			this.jPanel1.setLayout(new GridBagLayout());
			this.jPanel1.add(this.getJButton3(), new GridBagConstraints());
			this.jPanel1.add(this.getJButton4(), gridBagConstraints);
			this.jPanel1.add(this.getJButton5(), gridBagConstraints1);
		}
		return this.jPanel1;
	}

	/**
	 * This method initializes jButton3
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getJButton3() {
		if (this.jButton3 == null) {
			this.jButton3 = new JButton();
			this.jButton3.setText("Rewrite");
			this.jButton3.setPreferredSize(new Dimension(99, 29));
			this.jButton3.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Rewrite query

					RView.this.rewritingTextArea.setText("");
					RView.this.SQLTextArea.setText("");
					RView.this.answerTextArea.setText("");

					RView.this.query = RView.this.queryTextArea.getText().trim();
					RView.this.ontologyPath = RView.this.ontologyTextField.getText().trim();

					if (RView.this.ontologyPath.indexOf("/") == 0)
						RView.this.ontologyPath = "file://" + RView.this.ontologyPath;

					try {
						RView.this.jFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						long begin = System.currentTimeMillis();
						RView.this.rewriting = RView.m_model.rewriteQuery(RView.this.query, RView.this.ontologyPath);
						long end = System.currentTimeMillis();
						Collections.sort(RView.this.rewriting, new Comparator<Clause>() {
							public int compare(Clause c1, Clause c2) {
								return c1.m_canonicalRepresentation.compareTo(c2.m_canonicalRepresentation);
							}
						});

						for (Clause c : RView.this.rewriting)
							RView.this.rewritingTextArea.append(c.toString() + "\n");

						int size = 0;
						for (Clause c : RView.this.rewriting)
							size += c.toString().length();

						RView.this.rewritingTextArea.append("-------------------------------\n");
						if (RView.this.rewriting.size() == 1)
							RView.this.rewritingTextArea.append("1 CQ (" + size + " symbols) in " + Long.toString(end - begin) + " millisec.");
						else
							RView.this.rewritingTextArea.append(RView.this.rewriting.size() + " CQs (" + size + " symbols) in " + Long.toString(end - begin)
									+ " millisec.");

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						RView.this.jOptionPane = RView.this.getJOptionPane();
						JOptionPane.showMessageDialog(RView.this.jFrame, e1.toString(), "Error", JOptionPane.ERROR_MESSAGE);
						e1.printStackTrace();
						RView.this.ontologyPath = "";
						RView.this.query = "";
					} finally {
						RView.this.jFrame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					}
				}

			});
		}
		return this.jButton3;
	}

	/**
	 * This method initializes jButton4
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getJButton4() {
		if (this.jButton4 == null) {
			this.jButton4 = new JButton();
			this.jButton4.setText("SQLify");
			this.jButton4.setPreferredSize(new Dimension(99, 29));
			this.jButton4.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					RView.this.jButton3.doClick();
					RView.this.mappingsPath = RView.this.mappingsTextField.getText().trim();
					RView.this.SQLTextArea.setText("");
					RView.this.answerTextArea.setText("");
					try {
						RView.this.jFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						String SQLQuery = RView.m_model.getSQLQuery(RView.this.mappingsPath, RView.this.rewriting);
						if (SQLQuery == null) {
							RView.this.jOptionPane = RView.this.getJOptionPane();
							JOptionPane.showMessageDialog(RView.this.jFrame, "Rewriting is not a UCQs.", "Warning", JOptionPane.INFORMATION_MESSAGE);
						} else if (SQLQuery.equals("")) {
							RView.this.jOptionPane = RView.this.getJOptionPane();
							JOptionPane.showMessageDialog(RView.this.jFrame, "Empty SQL query (not enough mappings).", "Warning",
									JOptionPane.INFORMATION_MESSAGE);
						} else
							RView.this.SQLTextArea.setText(SQLQuery);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						RView.this.jOptionPane = RView.this.getJOptionPane();
						JOptionPane.showMessageDialog(RView.this.jFrame, e1.toString(), "Error", JOptionPane.ERROR_MESSAGE);
						e1.printStackTrace();
					} finally {
						RView.this.jFrame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					}
				}
			});
		}
		return this.jButton4;
	}

	/**
	 * This method initializes jButton5
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getJButton5() {
		if (this.jButton5 == null) {
			this.jButton5 = new JButton();
			this.jButton5.setText("Evaluate");
			this.jButton5.setPreferredSize(new Dimension(99, 29));
			this.jButton5.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					RView.this.jButton4.doClick();
					RView.this.answerTextArea.setText("");
					RView.this.jFrame.paint(RView.this.jFrame.getGraphics());
					if (!RView.this.SQLTextArea.getText().trim().equals(""))
						new Thread() {
							@Override
							public void run() {
								try {
									RView.this.databaseConnectionData[0] = RView.this.databaseURL.getText().trim();
									RView.this.databaseConnectionData[1] = RView.this.databaseName.getText().trim();
									RView.this.databaseConnectionData[2] = RView.this.username.getText().trim();
									RView.this.databaseConnectionData[3] = new String(RView.this.password.getPassword()).trim();
									RView.this.jFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
									RView.m_model.evaluateSQLQuery(RView.this.databaseConnectionData, RView.this.SQLTextArea.getText().trim(),
											RView.this.answerTextArea);
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									RView.this.jOptionPane = RView.this.getJOptionPane();
									JOptionPane.showMessageDialog(RView.this.jFrame, e1.toString(), "Error", JOptionPane.ERROR_MESSAGE);
									e1.printStackTrace();
								} finally {
									RView.this.jFrame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
								}
							}
						}.start();
				}

			});
		}
		return this.jButton5;
	}

	/**
	 * This method initializes jFileChooser
	 * 
	 * @return javax.swing.JFileChooser
	 */
	private JFileChooser getJFileChooser() {
		if (this.jFileChooser == null)
			this.jFileChooser = new JFileChooser();
		return this.jFileChooser;
	}

	/**
	 * This method initializes ontologyTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getOntologyTextField() {
		if (this.ontologyTextField == null) {
			this.ontologyTextField = new JTextField();
			this.ontologyTextField.setText("/Users/hekanibru/Documents/DPhil/Prototype/Tokyo/JapaneseWordNet/wnfull_modified_japanese.owl");
			this.ontologyTextField.setPreferredSize(new Dimension(709, 28));
		}
		return this.ontologyTextField;
	}

	/**
	 * This method initializes queryTextArea
	 * 
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getQueryTextArea() {
		if (this.queryTextArea == null) {
			this.queryTextArea = new JTextArea();
			this.queryTextArea.setText("Q(?0,?3) <- containsWordSense(?0,?1), word(?1,?2), lexicalForm(?2,\"bank\"), gloss(?0,?3)");
		}
		return this.queryTextArea;
	}

	/**
	 * This method initializes jOptionPane
	 * 
	 * @return javax.swing.JOptionPane
	 */
	private JOptionPane getJOptionPane() {
		if (this.jOptionPane == null) {
			this.jOptionPane = new JOptionPane();
			this.jOptionPane.setMessageType(JOptionPane.ERROR_MESSAGE);
			this.jOptionPane.setMessage("");
		}
		return this.jOptionPane;
	}

	/**
	 * This method initializes jPanel9
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel9() {
		if (this.jPanel9 == null) {
			this.jPanel9 = new JPanel();
			this.jPanel9.setLayout(new BorderLayout());
			this.jPanel9.setPreferredSize(new Dimension(1594, 384));
			this.jPanel9.add(this.getJSplitPane1(), BorderLayout.CENTER);
			this.jPanel9.add(this.getJPanel6(), BorderLayout.NORTH);
		}
		return this.jPanel9;
	}

	/**
	 * This method initializes jPanel10
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel10() {
		if (this.jPanel10 == null) {
			this.jPanel10 = new JPanel();
			this.jPanel10.setLayout(new BorderLayout());
			this.jPanel10.add(this.getQueryPanel(), BorderLayout.CENTER);
			this.jPanel10.add(this.getOntologyPanel(), BorderLayout.NORTH);
			this.jPanel10.add(this.getMappingsPanel(), BorderLayout.SOUTH);
		}
		return this.jPanel10;
	}

	/**
	 * This method initializes jMenu
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenu() {
		if (this.jMenu == null) {
			this.jMenu = new JMenu();
			this.jMenu.setText("REQUIEM");
			this.jMenu.add(this.getJMenuItem());
			this.jMenu.add(this.getJSeparator());
			this.jMenu.add(this.getJMenuItem1());
		}
		return this.jMenu;
	}

	/**
	 * This method initializes jMenuItem
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItem() {
		if (this.jMenuItem == null) {
			this.jMenuItem = new JMenuItem();
			this.jMenuItem.setText("About");
			this.jMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					JDialog aboutDialog = RView.this.getAboutDialog();
					aboutDialog.pack();
					Point loc = RView.this.getJFrame().getLocation();
					loc.translate(20, 20);
					aboutDialog.setLocation(loc);
					aboutDialog.setVisible(true);
				}
			});
		}
		return this.jMenuItem;
	}

	/**
	 * This method initializes jMenuItem1
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItem1() {
		if (this.jMenuItem1 == null) {
			this.jMenuItem1 = new JMenuItem();
			this.jMenuItem1.setText("Quit");
			this.jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					System.exit(0);
				}
			});
		}
		return this.jMenuItem1;
	}

	/**
	 * This method initializes jJMenuBar
	 * 
	 * @return javax.swing.JMenuBar
	 */
	private JMenuBar getJJMenuBar() {
		if (this.jJMenuBar == null) {
			this.jJMenuBar = new JMenuBar();
			this.jJMenuBar.add(this.getJMenu());
			this.jJMenuBar.add(this.getJMenu1());
			this.jJMenuBar.add(this.getJMenu2());
			this.jJMenuBar.add(this.getJMenu4());
		}
		return this.jJMenuBar;
	}

	/**
	 * This method initializes jMenu1
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenu1() {
		if (this.jMenu1 == null) {
			this.jMenu1 = new JMenu();
			this.jMenu1.setText("Query");
			this.jMenu1.add(this.getJMenuItem2());
			this.jMenu1.add(this.getJMenuItem3());
		}
		return this.jMenu1;
	}

	/**
	 * This method initializes jMenuItem2
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItem2() {
		if (this.jMenuItem2 == null) {
			this.jMenuItem2 = new JMenuItem();
			this.jMenuItem2.setText("Open File...");
			this.jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					RView.this.jFileChooser = null;
					RView.this.jFileChooser = RView.this.getJFileChooser();

					ExtensionFileFilter filter = new ExtensionFileFilter();
					filter.addExtension("txt", false);
					filter.setDescription("Text files");
					RView.this.jFileChooser.setFileFilter(filter);

					int value = RView.this.jFileChooser.showOpenDialog(RView.this.jFrame);

					if (value == JFileChooser.APPROVE_OPTION) {
						// Load mappings
						RView.this.queryTextArea.setText("");
						try {
							RView.this.queryTextArea.setText(RView.m_model.loadFile(RView.this.jFileChooser.getSelectedFile()));
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							RView.this.jOptionPane = RView.this.getJOptionPane();
							JOptionPane.showMessageDialog(RView.this.jFrame, e1.toString(), "Error", JOptionPane.ERROR_MESSAGE);
							e1.printStackTrace();

						}
					}
				}
			});
		}
		return this.jMenuItem2;
	}

	/**
	 * This method initializes jMenuItem3
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItem3() {
		if (this.jMenuItem3 == null) {
			this.jMenuItem3 = new JMenuItem();
			this.jMenuItem3.setText("Save As...");
			this.jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					RView.this.jFileChooser = null;
					RView.this.jFileChooser = RView.this.getJFileChooser();

					int returnValue = RView.this.jFileChooser.showSaveDialog(RView.this.jFrame);
					if (returnValue == JFileChooser.APPROVE_OPTION)
						try {
							RView.m_model.saveFile(RView.this.jFileChooser.getSelectedFile(), RView.this.queryTextArea.getText());
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							RView.this.jOptionPane = RView.this.getJOptionPane();
							JOptionPane.showMessageDialog(RView.this.jFrame, e1.toString(), "Error", JOptionPane.ERROR_MESSAGE);

							e1.printStackTrace();
						}
				}
			});
		}
		return this.jMenuItem3;
	}

	/**
	 * This method initializes jMenu2
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenu2() {
		if (this.jMenu2 == null) {
			this.jMenu2 = new JMenu();
			this.jMenu2.setText("Rewriting");
			this.jMenu2.add(this.getJMenuItem4());
			this.jMenu2.add(this.getJMenuItem7());
		}
		return this.jMenu2;
	}

	/**
	 * This method initializes jMenuItem4
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItem4() {
		if (this.jMenuItem4 == null) {
			this.jMenuItem4 = new JMenuItem();
			this.jMenuItem4.setText("Save Rewriting As...");
			this.jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					RView.this.jFileChooser = null;
					RView.this.jFileChooser = RView.this.getJFileChooser();

					int returnValue = RView.this.jFileChooser.showSaveDialog(RView.this.jFrame);
					if (returnValue == JFileChooser.APPROVE_OPTION)
						try {
							RView.m_model.saveFile(RView.this.jFileChooser.getSelectedFile(), RView.this.rewritingTextArea.getText());
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							RView.this.jOptionPane = RView.this.getJOptionPane();
							JOptionPane.showMessageDialog(RView.this.jFrame, e1.toString(), "Error", JOptionPane.ERROR_MESSAGE);

							e1.printStackTrace();
						}
				}
			});
		}
		return this.jMenuItem4;
	}

	/**
	 * This method initializes jMenuItem7
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItem7() {
		if (this.jMenuItem7 == null) {
			this.jMenuItem7 = new JMenuItem();
			this.jMenuItem7.setText("Save SQL As...");
			this.jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					RView.this.jFileChooser = null;
					RView.this.jFileChooser = RView.this.getJFileChooser();

					int returnValue = RView.this.jFileChooser.showSaveDialog(RView.this.jFrame);
					if (returnValue == JFileChooser.APPROVE_OPTION)
						try {
							RView.m_model.saveFile(RView.this.jFileChooser.getSelectedFile(), RView.this.SQLTextArea.getText());
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							RView.this.jOptionPane = RView.this.getJOptionPane();
							JOptionPane.showMessageDialog(RView.this.jFrame, e1.toString(), "Error", JOptionPane.ERROR_MESSAGE);

							e1.printStackTrace();
						}
				}
			});
		}
		return this.jMenuItem7;
	}

	/**
	 * This method initializes SQLPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getSQLPanel() {
		if (this.SQLPanel == null) {
			this.SQLPanel = new JPanel();
			this.SQLPanel.setLayout(new BorderLayout());
			this.SQLPanel.setBorder(BorderFactory.createTitledBorder(null, "SQL", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font(
					"Lucida Grande", Font.PLAIN, 13), Color.black));
			this.SQLPanel.add(this.getJScrollPane21(), java.awt.BorderLayout.CENTER);
		}
		return this.SQLPanel;
	}

	/**
	 * This method initializes jScrollPane21
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane21() {
		if (this.jScrollPane21 == null) {
			this.jScrollPane21 = new JScrollPane();
			this.jScrollPane21.setViewportView(this.getSQLTextArea());
		}
		return this.jScrollPane21;
	}

	/**
	 * This method initializes SQLTextArea
	 * 
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getSQLTextArea() {
		if (this.SQLTextArea == null) {
			this.SQLTextArea = new JTextArea();
			this.SQLTextArea.setEnabled(true);
			this.SQLTextArea.setEditable(false);
		}
		return this.SQLTextArea;
	}

	/**
	 * This method initializes jSplitPane
	 * 
	 * @return javax.swing.JSplitPane
	 */
	private JSplitPane getJSplitPane() {
		if (this.jSplitPane == null) {
			this.jSplitPane = new JSplitPane();
			this.jSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
			this.jSplitPane.setBottomComponent(this.getSQLPanel());
			this.jSplitPane.setTopComponent(this.getRewritingPanel());
			this.jSplitPane.setResizeWeight(0.5);

		}
		return this.jSplitPane;
	}

	/**
	 * This method initializes jSplitPane1
	 * 
	 * @return javax.swing.JSplitPane
	 */
	private JSplitPane getJSplitPane1() {
		if (this.jSplitPane1 == null) {
			this.jSplitPane1 = new JSplitPane();
			this.jSplitPane1.setDividerLocation(500);
			this.jSplitPane1.setPreferredSize(new Dimension(1094, 113));
			this.jSplitPane1.setLeftComponent(this.getJSplitPane());
			this.jSplitPane1.setRightComponent(this.getAnswerPanel());
			this.jSplitPane1.setResizeWeight(0.5);
		}
		return this.jSplitPane1;
	}

	/**
	 * This method initializes answerPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getAnswerPanel() {
		if (this.answerPanel == null) {
			this.answerPanel = new JPanel();
			this.answerPanel.setLayout(new BorderLayout());
			this.answerPanel.setBorder(BorderFactory.createTitledBorder(null, "Answer", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
					new Font("Lucida Grande", Font.PLAIN, 13), Color.black));
			this.answerPanel.add(this.getJScrollPane22(), java.awt.BorderLayout.CENTER);
		}
		return this.answerPanel;
	}

	/**
	 * This method initializes jScrollPane22
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane22() {
		if (this.jScrollPane22 == null) {
			this.jScrollPane22 = new JScrollPane();
			this.jScrollPane22.setViewportView(this.getAnswerTextArea());
		}
		return this.jScrollPane22;
	}

	/**
	 * This method initializes answerTextArea
	 * 
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getAnswerTextArea() {
		if (this.answerTextArea == null) {
			this.answerTextArea = new JTextArea();
			this.answerTextArea.setEnabled(true);
			this.answerTextArea.setEditable(false);
		}
		return this.answerTextArea;
	}

	/**
	 * This method initializes jMenu4
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenu4() {
		if (this.jMenu4 == null) {
			this.jMenu4 = new JMenu();
			this.jMenu4.setText("Answer");
			this.jMenu4.add(this.getJMenuItem8());
		}
		return this.jMenu4;
	}

	/**
	 * This method initializes jMenuItem8
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItem8() {
		if (this.jMenuItem8 == null) {
			this.jMenuItem8 = new JMenuItem();
			this.jMenuItem8.setText("Save As...");
			this.jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					RView.this.jFileChooser = null;
					RView.this.jFileChooser = RView.this.getJFileChooser();

					int returnValue = RView.this.jFileChooser.showSaveDialog(RView.this.jFrame);
					if (returnValue == JFileChooser.APPROVE_OPTION)
						try {
							RView.m_model.saveFile(RView.this.jFileChooser.getSelectedFile(), RView.this.answerTextArea.getText());
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							RView.this.jOptionPane = RView.this.getJOptionPane();
							JOptionPane.showMessageDialog(RView.this.jFrame, e1.toString(), "Error", JOptionPane.ERROR_MESSAGE);

							e1.printStackTrace();
						}
				}
			});
		}
		return this.jMenuItem8;
	}

	/**
	 * This method initializes jSeparator
	 * 
	 * @return javax.swing.JSeparator
	 */
	private JSeparator getJSeparator() {
		if (this.jSeparator == null)
			this.jSeparator = new JSeparator();
		return this.jSeparator;
	}

	/**
	 * Launches this application
	 */
	public void run() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				RView application = new RView();
				application.getJFrame().setVisible(true);
			}
		});
	}

	/**
	 * This method initializes mappingsPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getMappingsPanel() {
		if (this.mappingsPanel == null) {
			this.mappingsPanel = new JPanel();
			this.mappingsPanel.setLayout(new BorderLayout());
			this.mappingsPanel.setName("");
			this.mappingsPanel.setBorder(BorderFactory.createTitledBorder(null, "Mappings", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
					new Font("Lucida Grande", Font.PLAIN, 13), Color.black));
			this.mappingsPanel.add(this.getJPanel51(), java.awt.BorderLayout.EAST);
			this.mappingsPanel.add(this.getMappingsTextField(), java.awt.BorderLayout.CENTER);
		}
		return this.mappingsPanel;
	}

	/**
	 * This method initializes jPanel51
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel51() {
		if (this.jPanel51 == null) {
			this.jPanel51 = new JPanel();
			this.jPanel51.setLayout(new BorderLayout());
			this.jPanel51.add(this.getJButton1(), java.awt.BorderLayout.EAST);
		}
		return this.jPanel51;
	}

	/**
	 * This method initializes jButton1
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getJButton1() {
		if (this.jButton1 == null) {
			this.jButton1 = new JButton();
			this.jButton1.setText("Choose...");
			this.jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					RView.this.jFileChooser = null;
					RView.this.jFileChooser = RView.this.getJFileChooser();

					ExtensionFileFilter filter = new ExtensionFileFilter();
					filter.addExtension("txt", false);
					filter.setDescription("Text files");
					RView.this.jFileChooser.setFileFilter(filter);

					int value = RView.this.jFileChooser.showOpenDialog(RView.this.jFrame);

					if (value == JFileChooser.APPROVE_OPTION) {
						RView.this.mappingsPath = RView.this.jFileChooser.getSelectedFile().getAbsolutePath();
						RView.this.mappingsTextField.setText(RView.this.mappingsPath);
					}

				}
			});
		}
		return this.jButton1;
	}

	/**
	 * This method initializes mappingsTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getMappingsTextField() {
		if (this.mappingsTextField == null) {
			this.mappingsTextField = new JTextField();
			this.mappingsTextField.setText("/Users/hekanibru/Documents/DPhil/Prototype/Tokyo/JapaneseWordNet/mappings_japanese.txt");
		}
		return this.mappingsTextField;
	}

	/**
	 * This method initializes jPanel6
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel6() {
		if (this.jPanel6 == null) {
			this.jPanel6 = new JPanel();
			this.jPanel6.setLayout(new BorderLayout());
			this.jPanel6.add(this.getJPanel10(), BorderLayout.CENTER);
			this.jPanel6.add(this.getJPanel7(), BorderLayout.SOUTH);
			this.jPanel6.add(this.getDBPanel(), BorderLayout.EAST);
		}
		return this.jPanel6;
	}

	/**
	 * This method initializes jPanel7
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel7() {
		if (this.jPanel7 == null) {
			this.jPanel7 = new JPanel();
			this.jPanel7.setLayout(new BorderLayout());
			this.jPanel7.add(this.getJPanel1(), BorderLayout.CENTER);
		}
		return this.jPanel7;
	}
}
