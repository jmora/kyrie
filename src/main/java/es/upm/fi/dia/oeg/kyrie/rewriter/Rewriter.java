package es.upm.fi.dia.oeg.kyrie.rewriter;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

import es.upm.fi.dia.oeg.kyrie.optimizer.Optimizer;

public class Rewriter {

	private static Logger logger = Logger.getLogger(Rewriter.class.getName());

	private ArrayList<Clause> presaturation;
	protected static final TermFactory m_termFactory = new TermFactory();
	public static final Optimizer m_optimizer = new Optimizer(Rewriter.m_termFactory);
	public static final Saturator m_semisaturator = new Saturator(Rewriter.m_termFactory, Rewriter.m_optimizer);

	public static String getontostring(ArrayList<Clause> ontology, Logger clog) {
		if (!clog.isEnabledFor(clog.getEffectiveLevel()))
			return "";
		String ontostring = "\n";
		for (Clause c : ontology)
			ontostring += c.toString() + "\n";
		return ontostring;
	}

	private Collection<String> mappedPredicates;

	public Rewriter() {
		super();
	}

	public Rewriter(ArrayList<Clause> clauses) {
		this.mappedPredicates = null;
		Rewriter.logger.info("Loading completed (" + clauses.size() + " clauses)");
		Rewriter.logger.debug(Rewriter.getontostring(clauses, Rewriter.logger));
		this.presaturation = Rewriter.m_semisaturator.saturate(clauses, new SelectionFunctionPresaturateKeep());
		Rewriter.logger.debug("Saturation completed (" + this.presaturation.size() + " clauses): " + Rewriter.getontostring(this.presaturation, Rewriter.logger));
		this.presaturation = Rewriter.m_semisaturator.saturate(this.presaturation, new SelectionFunctionPresaturateAux(Rewriter.m_optimizer.stopAuxPoints(this.presaturation)));
		Rewriter.logger.debug("Clean1 (" + this.presaturation.size() + " clauses): " + Rewriter.getontostring(this.presaturation, Rewriter.logger));
		this.presaturation = Rewriter.m_optimizer.condensate(this.presaturation);
		Rewriter.logger.debug("Clean2 (" + this.presaturation.size() + " clauses): " + Rewriter.getontostring(this.presaturation, Rewriter.logger));
		this.presaturation = Rewriter.m_optimizer.querySubsumptionCheck(this.presaturation);
		Rewriter.logger.info("Preprocessing completed (" + this.presaturation.size() + " clauses)");
		Rewriter.logger.debug(Rewriter.getontostring(this.presaturation, Rewriter.logger));
	}

	public Rewriter(ArrayList<Clause> clauses, Collection<String> mappedPredicates) {
		this.mappedPredicates = mappedPredicates;
		Rewriter.logger.debug("\nOntology loaded (" + clauses.size() + " clauses): " + Rewriter.getontostring(clauses, Rewriter.logger));
		this.presaturation = Rewriter.m_semisaturator.saturate(clauses, new SelectionFunctionPresaturateKeep());
		Rewriter.logger.debug("Saturation completed (" + this.presaturation.size() + " clauses): " + Rewriter.getontostring(this.presaturation, Rewriter.logger));
		mappedPredicates.addAll(Rewriter.m_optimizer.stopPoints(this.presaturation, mappedPredicates));
		this.presaturation = Rewriter.m_semisaturator.saturate(this.presaturation, new SelectionFunctionPresaturateMap(mappedPredicates));
		Rewriter.logger.debug("Clean maps (" + this.presaturation.size() + " clauses): " + Rewriter.getontostring(this.presaturation, Rewriter.logger));
		this.presaturation = Rewriter.m_optimizer.condensate(this.presaturation);
		Rewriter.logger.debug("Clean2 (" + this.presaturation.size() + " clauses): " + Rewriter.getontostring(this.presaturation, Rewriter.logger));
		this.presaturation = Rewriter.m_optimizer.querySubsumptionCheck(this.presaturation);
		Rewriter.logger.info("Finished preprocessing of " + this.presaturation.size() + " clauses");
		Rewriter.logger.debug(Rewriter.getontostring(this.presaturation, Rewriter.logger));
	}

	public ArrayList<Clause> rewrite(ArrayList<Clause> query) {
		Rewriter.logger.info("Reloading completed (" + this.presaturation.size() + " clauses)");
		ArrayList<Clause> optQuery = Rewriter.m_optimizer.condensate(query);
		optQuery = Rewriter.m_optimizer.querySubsumptionCheck(optQuery);
		ArrayList<Clause> rewriting = this.presaturation;
		if (this.mappedPredicates != null)
			rewriting = Rewriter.m_optimizer.pruneWithDependencyGraphAndMappings(optQuery, rewriting, this.mappedPredicates, false);
		else
			rewriting = Rewriter.m_optimizer.pruneWithDependencyGraph(optQuery, rewriting);
		Rewriter.logger.debug("Reachability (" + rewriting.size() + " clauses): " + Rewriter.getontostring(rewriting, Rewriter.logger));
		rewriting = Rewriter.m_semisaturator.saturate(rewriting, optQuery, new SelectionFunctionPostsaturate(), true);
		Rewriter.logger.debug("With query (" + rewriting.size() + " clauses): " + Rewriter.getontostring(rewriting, Rewriter.logger));
		if (this.mappedPredicates != null) {
			Collection<String> maps = new ArrayList<String>(this.mappedPredicates);
			maps.addAll(Rewriter.m_optimizer.stopExistentialPoints(rewriting, this.mappedPredicates));
			rewriting = Rewriter.m_semisaturator.saturate(rewriting, new SelectionFunctionSaturateMap(maps));
		} else
			rewriting = Rewriter.m_semisaturator.saturate(rewriting, new SelectionFunctionPresaturateAux(new ArrayList<String>()));
		if (this.mappedPredicates != null)
			rewriting = Rewriter.m_optimizer.pruneWithDependencyGraphAndMappings("Q", rewriting, this.mappedPredicates, false);
		else
			rewriting = Rewriter.m_optimizer.pruneWithDependencyGraph("Q", rewriting);
		Rewriter.logger.info("Saturation completed (" + rewriting.size() + " clauses)");
		Rewriter.logger.debug(Rewriter.getontostring(rewriting, Rewriter.logger));
		rewriting = Rewriter.m_semisaturator.inclusiveUnfold(rewriting, Rewriter.m_optimizer.stopExistentialPoints(rewriting));
		Rewriter.logger.info("Unfolding completed (" + rewriting.size() + " clauses)");
		Rewriter.logger.debug(Rewriter.getontostring(rewriting, Rewriter.logger));
		return rewriting;
	}
}
