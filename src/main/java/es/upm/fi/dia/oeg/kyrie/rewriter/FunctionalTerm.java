package es.upm.fi.dia.oeg.kyrie.rewriter;

import java.util.Arrays;
import java.util.HashMap;


public class FunctionalTerm extends Term {
	protected String m_name;
	protected Term[] m_arguments;

	public FunctionalTerm(String name, Term[] arguments) {
		this.m_name = name;
		this.m_arguments = arguments;
	}

	@Override
	public String getName() {
		return this.m_name;
	}

	@Override
	public void setName(String name) {
		this.m_name = name;
	}

	@Override
	public int getArity() {
		return this.m_arguments.length;
	}

	@Override
	public Term getArgument(int argumentIndex) {
		return this.m_arguments[argumentIndex];
	}

	// public boolean equals(Object o){
	// if(!(o instanceof Term))
	// return false;
	// Term t = (Term) o;
	// List<Term> tArgs = Arrays.asList(t.getArguments());
	// List<Term> thisArgs = Arrays.asList(this.getArguments());
	// if (!(tArgs.containsAll(thisArgs) && thisArgs.containsAll(tArgs)))
	// return false;
	// return t.getName().equals(this.getName()); //TODO: this is case
	// sensitive, how do you like it?
	// }

	@Override
	public Term[] getArguments() {
		return this.m_arguments;
	}

	@Override
	public boolean contains(Term term) {
		if (this.equals(term))
			return true;
		for (int index = this.m_arguments.length - 1; index >= 0; --index)
			if (this.m_arguments[index].contains(term))
				return true;
		return false;
	}

	@Override
	public Term apply(Substitution substitution, TermFactory termFactory) {
		if (this.m_arguments.length == 0 || substitution.isEmpty())
			return this;
		else {
			Term[] arguments = new Term[this.m_arguments.length];
			for (int index = this.m_arguments.length - 1; index >= 0; --index)
				arguments[index] = this.m_arguments[index].apply(substitution, termFactory);
			return termFactory.getFunctionalTerm(this.m_name, arguments);
		}
	}

	@Override
	public Term offsetVariables(TermFactory termFactory, int offset) {
		if (this.m_arguments.length == 0)
			return this;
		else {
			Term[] arguments = new Term[this.m_arguments.length];
			for (int index = this.m_arguments.length - 1; index >= 0; --index)
				arguments[index] = this.m_arguments[index].offsetVariables(termFactory, offset);
			return termFactory.getFunctionalTerm(this.m_name, arguments);
		}
	}

	@Override
	public Term renameVariables(TermFactory termFactory, HashMap<Variable, Integer> mapping) {
		if (this.m_arguments.length == 0)
			return this;
		else {
			Term[] arguments = new Term[this.m_arguments.length];
			for (int index = this.m_arguments.length - 1; index >= 0; --index)
				arguments[index] = this.m_arguments[index].renameVariables(termFactory, mapping);
			return termFactory.getFunctionalTerm(this.m_name, arguments);
		}
	}

	@Override
	public int getMinVariableIndex() {
		int minVariableIndex = Integer.MAX_VALUE;
		for (int index = this.m_arguments.length - 1; index >= 0; --index)
			minVariableIndex = Math.min(minVariableIndex, this.m_arguments[index].getMinVariableIndex());
		return minVariableIndex;
	}

	@Override
	public void toString(StringBuffer buffer) {
		buffer.append(this.getName());
		if (this.m_arguments.length > 0) {
			buffer.append('(');
			for (int index = 0; index < this.m_arguments.length; index++) {
				if (index != 0)
					buffer.append(',');
				this.m_arguments[index].toString(buffer);
			}
			buffer.append(')');
		}
	}

	@Override
	public int getDepth() {
		return this.getDepth(this);
	}

	private int getDepth(Term t) {
		if (t.getArity() == 0)
			return 0;
		else {
			int[] depths = new int[t.getArity()];
			for (int i = 0; i < t.getArity(); i++)
				depths[i] = this.getDepth(t.getArgument(i));
			Arrays.sort(depths);
			return 1 + depths[t.getArity() - 1];
		}
	}

	@Override
	public Term getVariableOrConstant() {
		if (this.getArity() == 0)
			return this;
		else
			return this.m_arguments[0].getVariableOrConstant(); // All
																// functional
																// terms have at
																// most one
																// element
	}

	@Override
	public String getFunctionalPrefix() {
		if (this.getArity() == 0)
			return this.m_name;
		else {
			String arguments = "";
			for (int i = 0; i < this.m_arguments.length; i++)
				arguments += this.m_arguments[i].getFunctionalPrefix();
			return this.m_name + arguments;
		}
	}
}
