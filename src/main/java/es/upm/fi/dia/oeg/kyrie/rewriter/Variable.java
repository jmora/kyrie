package es.upm.fi.dia.oeg.kyrie.rewriter;

import java.util.HashMap;


public class Variable extends Term {
	protected int m_index;

	protected Variable(int index) {
		this.m_index = index;
	}

	@Override
	public String getName() {
		return "?" + this.m_index;
	}

	@Override
	public int getArity() {
		return 0;
	}

	@Override
	public Term getArgument(int argumentIndex) {
		throw new IndexOutOfBoundsException();
	}

	@Override
	public Term[] getArguments() {
		throw new IndexOutOfBoundsException();
		// return new Term[0];//jmora 20101111
	}

	@Override
	public boolean contains(Term term) {
		return this.equals(term);
	}

	@Override
	public Term apply(Substitution substitution, TermFactory termFactory) {
		Term result = substitution.get(this);
		if (result != null)
			return result;
		else
			return this;
	}

	@Override
	public Term offsetVariables(TermFactory termFactory, int offset) {
		return termFactory.getVariable(this.m_index + offset);
	}

	@Override
	public Term renameVariables(TermFactory termFactory, HashMap<Variable, Integer> mapping) {
		return termFactory.getVariable(mapping.get(this));
	}

	@Override
	public int getMinVariableIndex() {
		return this.m_index;
	}

	@Override
	public void toString(StringBuffer buffer) {
		buffer.append(this.getName());
	}

	@Override
	public int getDepth() {
		return 0;
	}

	@Override
	public Term getVariableOrConstant() {
		return this;
	}

	@Override
	public String getFunctionalPrefix() {
		return "";
	}

	public void setIndex(int index) {
		this.m_index = index;
	}

	@Override
	public boolean equals(Object that) {
		if (that instanceof Variable)
			return this.getName().equals(((Variable) that).getName());
		return false;
	}

	@Override
	public int hashCode() {
		return this.m_index;
	}

	@Override
	// I hope the String is a number, or I'll ignore you
	public void setName(String name) {
		try {
			this.m_index = Integer.parseInt(name);
		} catch (NumberFormatException e) {
			return;
		}
	}
}
