package es.upm.fi.dia.oeg.kyrie.rewriter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import es.upm.fi.dia.oeg.kyrie.optimizer.Optimizer;


public class ResolutionManager {

	// private class PendingIterable implements Iterable<Clause> {
	// private ResolutionManager rm;
	//
	// public PendingIterable(ResolutionManager rm) {
	// this.rm = rm;
	// }
	//
	// @Override
	// public Iterator<Clause> iterator () {
	// return new PendingIterator(this.rm);
	// }
	//
	// }
	//
	// private class PendingIterator implements Iterator<Clause> {
	// private ResolutionManager rm;
	// private int arrayCounter = 0;
	// private int clauseCounter = 0;
	//
	// public PendingIterator(ResolutionManager rm) {
	// this.rm = rm;
	// this.arrayCounter = 0;
	// this.clauseCounter = 0;
	// }
	//
	// @Override
	// public boolean hasNext () {
	// int lastArray = this.rm.pending.size() - 1;
	// return this.arrayCounter < lastArray || this.arrayCounter == lastArray &&
	// this.clauseCounter < this.rm.pending.get(lastArray).size();
	// }
	//
	// @Override
	// public Clause next () {
	// Clause r =
	// this.rm.pending.get(this.arrayCounter).get(this.clauseCounter);
	// this.clauseCounter += 1;
	// // if (clauseCounter >= this.rm.pending.get(index))
	// return r;
	// }
	//
	// @Override
	// public void remove () {
	//
	// }
	// }

	private HashMap<Integer, ArrayList<Clause>> workedOff = new HashMap<Integer, ArrayList<Clause>>();
	private HashMap<Integer, ArrayList<Clause>> pending = new HashMap<Integer, ArrayList<Clause>>();
	// private static Logger logger =
	// Logger.getLogger(ResolutionManager.class.getName());
	private Optimizer optimizer;

	// private SelectionFunction sf;

	public ResolutionManager(ArrayList<Clause> clauses, Optimizer optimizer, SelectionFunction sf) {
		this.optimizer = optimizer;
		// this.sf = sf;
		for (Clause c : clauses) {
			int len = c.getBody().length;
			if (!this.pending.containsKey(len))
				this.pending.put(len, new ArrayList<Clause>());
			this.pending.get(len).add(c);
		}
	}

	public void addPending(Clause c) {
		Clause nc = this.optimizer.condensate(c);
		int len = nc.getBody().length;
		// check subsumption
		if (this.isSubsumed(nc, this.workedOff) || this.isSubsumed(nc, this.pending))
			return;
		if (!this.pending.containsKey(len))
			this.pending.put(len, new ArrayList<Clause>());
		this.pending.get(len).add(nc);
	}

	public void addWorked(Clause c) {
		int len = c.getBody().length;
		if (!this.workedOff.containsKey(len))
			this.workedOff.put(len, new ArrayList<Clause>());
		this.workedOff.get(len).add(c);
	}

	public ArrayList<Clause> getAllWorked() {
		ArrayList<Clause> result = new ArrayList<Clause>();
		for (Integer i : this.workedOff.keySet())
			result.addAll(this.workedOff.get(i));
		return result;
	}

	public boolean isPendingEmpty() {
		for (Integer i : new ArrayList<Integer>(this.pending.keySet()))
			if (this.pending.get(i).isEmpty())
				this.pending.remove(i);
			else
				return false;
		return this.pending.isEmpty();
	}

	private boolean isSubsumed(Clause oc, HashMap<Integer, ArrayList<Clause>> clauses) {
		int len = oc.getBody().length;
		boolean clauseSubsumption = true;
		ArrayList<Integer> lens = new ArrayList<Integer>(clauses.keySet());
		Collections.sort(lens);
		ArrayList<Clause> newclauses;
		int clen = lens.remove(0);
		while (clen < len) {
			// newclauses = new ArrayList<Clause>();
			for (Clause c : clauses.get(clen))
				if (c.subsumes(oc, clauseSubsumption))
					return true;
			// else if (!oc.subsumes(c, clauseSubsumption))
			// newclauses.add(c);
			// else
			// ResolutionManager.logger.debug("OMG this happened! " +
			// oc.toString() + " subsumes to " + c.toString());
			// clauses.put(clen, newclauses);
			if (lens.isEmpty())
				break;
			clen = lens.remove(0);
		}
		if (clen == len) {
			newclauses = new ArrayList<Clause>();
			for (Clause c : clauses.get(len))
				if (c.subsumes(oc, clauseSubsumption))
					return true;
				else if (!oc.subsumes(c, clauseSubsumption))
					newclauses.add(c);
			clauses.put(len, newclauses);
			if (!lens.isEmpty())
				clen = lens.remove(0);
		}
		while (clen > len) {
			newclauses = new ArrayList<Clause>();
			for (Clause c : clauses.get(clen))
				// if (c.subsumes(oc, clauseSubsumption)) {
				// ResolutionManager.logger.debug("OMG this happened " +
				// c.toString() + " subsumes to " + oc.toString());
				// return true;
				// } else
				if (!oc.subsumes(c, clauseSubsumption))
					newclauses.add(c);
			clauses.put(clen, newclauses);
			if (lens.isEmpty())
				break;
			clen = lens.remove(0);
		}
		return false;
	}

	public Clause removePending() {
		ArrayList<Integer> keys = new ArrayList<Integer>(this.pending.keySet());
		Collections.sort(keys);
		for (Integer i : keys)
			if (this.pending.get(i).isEmpty())
				this.pending.remove(i);
			else
				return this.pending.get(i).remove(0);
		return null;
	}

}
