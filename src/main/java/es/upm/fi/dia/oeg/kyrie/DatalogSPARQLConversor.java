package es.upm.fi.dia.oeg.kyrie;

import java.util.ArrayList;
import java.util.HashMap;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.sparql.algebra.Algebra;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpBGP;
import com.hp.hpl.jena.sparql.algebra.op.OpProject;
import com.hp.hpl.jena.sparql.core.BasicPattern;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;

import es.upm.fi.dia.oeg.kyrie.rewriter.Clause;
import es.upm.fi.dia.oeg.kyrie.rewriter.FunctionalTerm;
import es.upm.fi.dia.oeg.kyrie.rewriter.Term;
import es.upm.fi.dia.oeg.kyrie.rewriter.TermFactory;


/**
 * 
 */

/**
 * @author jmora
 * 
 */
public class DatalogSPARQLConversor {

	private static final TermFactory tf = new TermFactory();
	// private Correspondences replacements;
	private HashMap<String, String> replacements;
	private HashMap<String, String> irreplacements;
	private int lastVar;
	private PrefixMapping pm;

	public DatalogSPARQLConversor() {
		this.lastVar = 0;
		this.replacements = new HashMap<String, String>();
		this.irreplacements = new HashMap<String, String>();
	}

	public ArrayList<Query> datalogToSPARQL(ArrayList<Clause> datalog) {
		ArrayList<Query> res = new ArrayList<Query>();
		for (Clause c : datalog) {
			Query query = new Query();
			query.setQuerySelectType();
			for (Term t : c.getHead().getArguments())
				query.addResultVar(this.termToNode(t).getName());
			ElementTriplesBlock triplePattern = new ElementTriplesBlock();
			for (Term t : c.getBody())
				triplePattern.addTriple(this.termToTriple(t));
			query.setQueryPattern(triplePattern);
			res.add(query);
		}
		return res;
	}

	public ArrayList<Clause> sparqlToDatalog(Query query) throws ConversorException {
		this.pm = query.getPrefixMapping();
		Op op = Algebra.compile(query);
		if (!(op instanceof OpProject))
			throw new ConversorException("Unsupported query!");
		Op opProjectSubOp = ((OpProject) op).getSubOp();
		if (!(opProjectSubOp instanceof OpBGP))
			throw new ConversorException("Unsupported query!");
		BasicPattern bp = ((OpBGP) opProjectSubOp).getPattern();
		ArrayList<Term> queryHead = new ArrayList<Term>();
		for (Var v : ((OpProject) op).getVars()) {
			Term t = this.nodeToTerm(v);
			this.replacementsput(t.getName(), v.toString(this.pm));
			queryHead.add(t);
		}
		// tf.getFunctionalTerm("Q", queryHead.toArray(new
		// Term[queryHead.size()]));
		FunctionalTerm head = DatalogSPARQLConversor.tf.getFunctionalTerm("Q", queryHead.toArray(new Term[0]));
		ArrayList<Term> cq = new ArrayList<Term>();
		for (Triple t : bp)
			cq.add(this.tripleToTerm(t));
		ArrayList<Clause> res = new ArrayList<Clause>();
		res.add(new Clause(cq.toArray(new Term[0]), head));
		return res;
	}

	private Term tripleToTerm(Triple t) {
		if (this.nodeToTerm(t.getPredicate()).getName().equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) {
			Term ts[] = { this.nodeToTerm(t.getSubject()) };
			return DatalogSPARQLConversor.tf.getFunctionalTerm(this.nodeToTerm(t.getObject()).getName(), ts);
		}
		Term ts[] = { this.nodeToTerm(t.getSubject()), this.nodeToTerm(t.getObject()) };
		return DatalogSPARQLConversor.tf.getFunctionalTerm(this.nodeToTerm(t.getPredicate()).getName(), ts);
	}

	private Triple termToTriple(Term t) {
		if (t.getArguments().length == 1) {
			Node o = this.termToNode(t);
			Node s = this.termToNode(t.getArgument(0));
			Node p = this.termToNode(DatalogSPARQLConversor.tf.getConstant("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"));
			return Triple.create(s, p, o);
		}
		Node p = this.termToNode(t);
		Node s = this.termToNode(t.getArgument(0));
		Node o = this.termToNode(t.getArgument(1));
		return Triple.create(s, p, o);
	}

	private Term nodeToTerm(Node node) {
		Term res = DatalogSPARQLConversor.tf.getConstant("nil");
		if (node.isBlank())
			res = DatalogSPARQLConversor.tf.getConstant(node.getBlankNodeId().toString());
		else if (node.isLiteral())
			res = DatalogSPARQLConversor.tf.getConstant(node.toString());
		else if (node.isVariable()) {
			if (this.irreplacements.containsKey(node.toString())) {
				String vn = this.irreplacements.get(node.toString());
				return DatalogSPARQLConversor.tf.getVariable(Integer.parseInt(vn.substring(1)));
			}
			res = DatalogSPARQLConversor.tf.getVariable(this.lastVar++);
		} else if (node.isURI())
			res = DatalogSPARQLConversor.tf.getConstant(node.getURI());
		this.replacementsput(res.getName(), node.toString(this.pm));
		return res;
	}

	private Node termToNode(Term t) {
		if (this.replacements.containsKey(t.getName()))
			return MoreJena.create(this.pm, this.replacements.get(t.getName()));
		if (t.getName().startsWith("?")) {
			this.replacementsput(t.getName(), Node.createAnon().toString());
			return this.termToNode(t);
		}
		return Node.createURI(t.getName()); // FIXME: what else could a term be?
											// Maybe a variable, I guess.
	}

	// the order in replacements is: key=term.name value=node.toString
	private void replacementsput(String key, String value) {
		this.replacements.put(key, value);
		this.irreplacements.put(value, key);
	}
}
