// $ANTLR 3.1 DnD2D.g 2012-06-07 14:41:58

package es.upm.fi.dia.oeg.kyrie;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;


public class DnD2DLexer extends Lexer {
	public static final int T__26 = 26;
	public static final int T__25 = 25;
	public static final int T__24 = 24;
	public static final int T__23 = 23;
	public static final int T__22 = 22;
	public static final int T__21 = 21;
	public static final int T__20 = 20;
	public static final int FLOAT = 6;
	public static final int INT = 5;
	public static final int NUMERIC = 8;
	public static final int ID = 7;
	public static final int EOF = -1;
	public static final int T__19 = 19;
	public static final int WS = 9;
	public static final int T__16 = 16;
	public static final int T__15 = 15;
	public static final int T__18 = 18;
	public static final int T__17 = 17;
	public static final int T__12 = 12;
	public static final int T__11 = 11;
	public static final int T__14 = 14;
	public static final int T__13 = 13;
	public static final int COMMENT = 10;
	public static final int STRING = 4;

	// delegates
	// delegators

	public DnD2DLexer() {
		;
	}

	public DnD2DLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}

	public DnD2DLexer(CharStream input, RecognizerSharedState state) {
		super(input, state);

	}

	@Override
	public String getGrammarFileName() {
		return "DnD2D.g";
	}

	// $ANTLR start "T__11"
	public final void mT__11() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__11;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:7:7: ( '<-' )
			// DnD2D.g:7:9: '<-'
			{
				this.match("<-");

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__11"

	// $ANTLR start "T__12"
	public final void mT__12() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__12;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:8:7: ( ':-' )
			// DnD2D.g:8:9: ':-'
			{
				this.match(":-");

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__12"

	// $ANTLR start "T__13"
	public final void mT__13() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__13;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:9:7: ( '^' )
			// DnD2D.g:9:9: '^'
			{
				this.match('^');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__13"

	// $ANTLR start "T__14"
	public final void mT__14() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__14;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:10:7: ( ',' )
			// DnD2D.g:10:9: ','
			{
				this.match(',');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__14"

	// $ANTLR start "T__15"
	public final void mT__15() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__15;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:11:7: ( '(' )
			// DnD2D.g:11:9: '('
			{
				this.match('(');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__15"

	// $ANTLR start "T__16"
	public final void mT__16() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__16;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:12:7: ( ')' )
			// DnD2D.g:12:9: ')'
			{
				this.match(')');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__16"

	// $ANTLR start "T__17"
	public final void mT__17() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__17;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:13:7: ( '=' )
			// DnD2D.g:13:9: '='
			{
				this.match('=');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__17"

	// $ANTLR start "T__18"
	public final void mT__18() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__18;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:14:7: ( '!=' )
			// DnD2D.g:14:9: '!='
			{
				this.match("!=");

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__18"

	// $ANTLR start "T__19"
	public final void mT__19() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__19;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:15:7: ( '<' )
			// DnD2D.g:15:9: '<'
			{
				this.match('<');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__19"

	// $ANTLR start "T__20"
	public final void mT__20() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__20;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:16:7: ( '>' )
			// DnD2D.g:16:9: '>'
			{
				this.match('>');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__20"

	// $ANTLR start "T__21"
	public final void mT__21() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__21;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:17:7: ( '>=' )
			// DnD2D.g:17:9: '>='
			{
				this.match(">=");

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__21"

	// $ANTLR start "T__22"
	public final void mT__22() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__22;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:18:7: ( '<=' )
			// DnD2D.g:18:9: '<='
			{
				this.match("<=");

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__22"

	// $ANTLR start "T__23"
	public final void mT__23() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__23;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:19:7: ( 'LIKE' )
			// DnD2D.g:19:9: 'LIKE'
			{
				this.match("LIKE");

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__23"

	// $ANTLR start "T__24"
	public final void mT__24() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__24;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:20:7: ( 'IS' )
			// DnD2D.g:20:9: 'IS'
			{
				this.match("IS");

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__24"

	// $ANTLR start "T__25"
	public final void mT__25() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__25;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:21:7: ( 'NULL' )
			// DnD2D.g:21:9: 'NULL'
			{
				this.match("NULL");

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__25"

	// $ANTLR start "T__26"
	public final void mT__26() throws RecognitionException {
		try {
			int _type = DnD2DLexer.T__26;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:22:7: ( 'NOT' )
			// DnD2D.g:22:9: 'NOT'
			{
				this.match("NOT");

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__26"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = DnD2DLexer.ID;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:84:4: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '$' ) ( ( 'a'
			// .. 'z' | 'A' .. 'Z' ) | ( '0' .. '9' ) | '-' | '_' | '#' )* | '`'
			// (~ ( '\\'' | '\\n' | '\\r' | '`' ) ) ( (~ ( '\\'' | '\\n' | '\\r'
			// | '`' ) ) )* '`' | '?' ( '0' .. '9' )+ )
			int alt4 = 3;
			switch (this.input.LA(1)) {
				case '$':
				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'J':
				case 'K':
				case 'L':
				case 'M':
				case 'N':
				case 'O':
				case 'P':
				case 'Q':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
				case 'V':
				case 'W':
				case 'X':
				case 'Y':
				case 'Z':
				case '_':
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
				case 'g':
				case 'h':
				case 'i':
				case 'j':
				case 'k':
				case 'l':
				case 'm':
				case 'n':
				case 'o':
				case 'p':
				case 'q':
				case 'r':
				case 's':
				case 't':
				case 'u':
				case 'v':
				case 'w':
				case 'x':
				case 'y':
				case 'z': {
					alt4 = 1;
				}
					break;
				case '`': {
					alt4 = 2;
				}
					break;
				case '?': {
					alt4 = 3;
				}
					break;
				default:
					NoViableAltException nvae = new NoViableAltException("", 4, 0, this.input);

					throw nvae;
			}

			switch (alt4) {
				case 1:
				// DnD2D.g:84:6: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '$' ) ( (
				// 'a' .. 'z' | 'A' .. 'Z' ) | ( '0' .. '9' ) | '-' | '_' | '#'
				// )*
				{
					if (this.input.LA(1) == '$' || this.input.LA(1) >= 'A' && this.input.LA(1) <= 'Z' || this.input.LA(1) == '_' || this.input.LA(1) >= 'a'
							&& this.input.LA(1) <= 'z')
						this.input.consume();
					else {
						MismatchedSetException mse = new MismatchedSetException(null, this.input);
						this.recover(mse);
						throw mse;
					}

					// DnD2D.g:84:40: ( ( 'a' .. 'z' | 'A' .. 'Z' ) | ( '0' ..
					// '9' ) | '-' | '_' | '#' )*
					loop1: do {
						int alt1 = 6;
						switch (this.input.LA(1)) {
							case 'A':
							case 'B':
							case 'C':
							case 'D':
							case 'E':
							case 'F':
							case 'G':
							case 'H':
							case 'I':
							case 'J':
							case 'K':
							case 'L':
							case 'M':
							case 'N':
							case 'O':
							case 'P':
							case 'Q':
							case 'R':
							case 'S':
							case 'T':
							case 'U':
							case 'V':
							case 'W':
							case 'X':
							case 'Y':
							case 'Z':
							case 'a':
							case 'b':
							case 'c':
							case 'd':
							case 'e':
							case 'f':
							case 'g':
							case 'h':
							case 'i':
							case 'j':
							case 'k':
							case 'l':
							case 'm':
							case 'n':
							case 'o':
							case 'p':
							case 'q':
							case 'r':
							case 's':
							case 't':
							case 'u':
							case 'v':
							case 'w':
							case 'x':
							case 'y':
							case 'z': {
								alt1 = 1;
							}
								break;
							case '0':
							case '1':
							case '2':
							case '3':
							case '4':
							case '5':
							case '6':
							case '7':
							case '8':
							case '9': {
								alt1 = 2;
							}
								break;
							case '-': {
								alt1 = 3;
							}
								break;
							case '_': {
								alt1 = 4;
							}
								break;
							case '#': {
								alt1 = 5;
							}
								break;

						}

						switch (alt1) {
							case 1:
							// DnD2D.g:84:42: ( 'a' .. 'z' | 'A' .. 'Z' )
							{
								if (this.input.LA(1) >= 'A' && this.input.LA(1) <= 'Z' || this.input.LA(1) >= 'a' && this.input.LA(1) <= 'z')
									this.input.consume();
								else {
									MismatchedSetException mse = new MismatchedSetException(null, this.input);
									this.recover(mse);
									throw mse;
								}

							}
								break;
							case 2:
							// DnD2D.g:84:66: ( '0' .. '9' )
							{
								// DnD2D.g:84:66: ( '0' .. '9' )
								// DnD2D.g:84:67: '0' .. '9'
								{
									this.matchRange('0', '9');

								}

							}
								break;
							case 3:
							// DnD2D.g:84:79: '-'
							{
								this.match('-');

							}
								break;
							case 4:
							// DnD2D.g:84:85: '_'
							{
								this.match('_');

							}
								break;
							case 5:
							// DnD2D.g:84:91: '#'
							{
								this.match('#');

							}
								break;

							default:
								break loop1;
						}
					} while (true);

				}
					break;
				case 2:
				// DnD2D.g:85:4: '`' (~ ( '\\'' | '\\n' | '\\r' | '`' ) ) ( (~
				// ( '\\'' | '\\n' | '\\r' | '`' ) ) )* '`'
				{
					this.match('`');
					// DnD2D.g:85:8: (~ ( '\\'' | '\\n' | '\\r' | '`' ) )
					// DnD2D.g:85:9: ~ ( '\\'' | '\\n' | '\\r' | '`' )
					{
						if (this.input.LA(1) >= '\u0000' && this.input.LA(1) <= '\t' || this.input.LA(1) >= '\u000B' && this.input.LA(1) <= '\f'
								|| this.input.LA(1) >= '\u000E' && this.input.LA(1) <= '&' || this.input.LA(1) >= '(' && this.input.LA(1) <= '_'
								|| this.input.LA(1) >= 'a' && this.input.LA(1) <= '\uFFFE')
							this.input.consume();
						else {
							MismatchedSetException mse = new MismatchedSetException(null, this.input);
							this.recover(mse);
							throw mse;
						}

					}

					// DnD2D.g:85:32: ( (~ ( '\\'' | '\\n' | '\\r' | '`' ) )
					// )*
					loop2: do {
						int alt2 = 2;
						int LA2_0 = this.input.LA(1);

						if (LA2_0 >= '\u0000' && LA2_0 <= '\t' || LA2_0 >= '\u000B' && LA2_0 <= '\f' || LA2_0 >= '\u000E' && LA2_0 <= '&' || LA2_0 >= '('
								&& LA2_0 <= '_' || LA2_0 >= 'a' && LA2_0 <= '\uFFFE')
							alt2 = 1;

						switch (alt2) {
							case 1:
							// DnD2D.g:85:34: (~ ( '\\'' | '\\n' | '\\r' | '`'
							// ) )
							{
								// DnD2D.g:85:34: (~ ( '\\'' | '\\n' | '\\r' |
								// '`' ) )
								// DnD2D.g:85:35: ~ ( '\\'' | '\\n' | '\\r' |
								// '`' )
								{
									if (this.input.LA(1) >= '\u0000' && this.input.LA(1) <= '\t' || this.input.LA(1) >= '\u000B' && this.input.LA(1) <= '\f'
											|| this.input.LA(1) >= '\u000E' && this.input.LA(1) <= '&' || this.input.LA(1) >= '(' && this.input.LA(1) <= '_'
											|| this.input.LA(1) >= 'a' && this.input.LA(1) <= '\uFFFE')
										this.input.consume();
									else {
										MismatchedSetException mse = new MismatchedSetException(null, this.input);
										this.recover(mse);
										throw mse;
									}

								}

							}
								break;

							default:
								break loop2;
						}
					} while (true);

					this.match('`');

				}
					break;
				case 3:
				// DnD2D.g:86:4: '?' ( '0' .. '9' )+
				{
					this.match('?');
					// DnD2D.g:86:7: ( '0' .. '9' )+
					int cnt3 = 0;
					loop3: do {
						int alt3 = 2;
						int LA3_0 = this.input.LA(1);

						if (LA3_0 >= '0' && LA3_0 <= '9')
							alt3 = 1;

						switch (alt3) {
							case 1:
							// DnD2D.g:86:8: '0' .. '9'
							{
								this.matchRange('0', '9');

							}
								break;

							default:
								if (cnt3 >= 1)
									break loop3;
								EarlyExitException eee = new EarlyExitException(3, this.input);
								throw eee;
						}
						cnt3++;
					} while (true);

				}
					break;

			}
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "ID"

	// $ANTLR start "FLOAT"
	public final void mFLOAT() throws RecognitionException {
		try {
			int _type = DnD2DLexer.FLOAT;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:87:7: ( ( '0' .. '9' )+ '.' ( '0' .. '9' )+ )
			// DnD2D.g:87:9: ( '0' .. '9' )+ '.' ( '0' .. '9' )+
			{
				// DnD2D.g:87:9: ( '0' .. '9' )+
				int cnt5 = 0;
				loop5: do {
					int alt5 = 2;
					int LA5_0 = this.input.LA(1);

					if (LA5_0 >= '0' && LA5_0 <= '9')
						alt5 = 1;

					switch (alt5) {
						case 1:
						// DnD2D.g:87:10: '0' .. '9'
						{
							this.matchRange('0', '9');

						}
							break;

						default:
							if (cnt5 >= 1)
								break loop5;
							EarlyExitException eee = new EarlyExitException(5, this.input);
							throw eee;
					}
					cnt5++;
				} while (true);

				this.match('.');
				// DnD2D.g:87:25: ( '0' .. '9' )+
				int cnt6 = 0;
				loop6: do {
					int alt6 = 2;
					int LA6_0 = this.input.LA(1);

					if (LA6_0 >= '0' && LA6_0 <= '9')
						alt6 = 1;

					switch (alt6) {
						case 1:
						// DnD2D.g:87:26: '0' .. '9'
						{
							this.matchRange('0', '9');

						}
							break;

						default:
							if (cnt6 >= 1)
								break loop6;
							EarlyExitException eee = new EarlyExitException(6, this.input);
							throw eee;
					}
					cnt6++;
				} while (true);

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "FLOAT"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = DnD2DLexer.INT;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:88:5: ( ( '0' .. '9' )+ )
			// DnD2D.g:88:7: ( '0' .. '9' )+
			{
				// DnD2D.g:88:7: ( '0' .. '9' )+
				int cnt7 = 0;
				loop7: do {
					int alt7 = 2;
					int LA7_0 = this.input.LA(1);

					if (LA7_0 >= '0' && LA7_0 <= '9')
						alt7 = 1;

					switch (alt7) {
						case 1:
						// DnD2D.g:88:8: '0' .. '9'
						{
							this.matchRange('0', '9');

						}
							break;

						default:
							if (cnt7 >= 1)
								break loop7;
							EarlyExitException eee = new EarlyExitException(7, this.input);
							throw eee;
					}
					cnt7++;
				} while (true);

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "INT"

	// $ANTLR start "NUMERIC"
	public final void mNUMERIC() throws RecognitionException {
		try {
			int _type = DnD2DLexer.NUMERIC;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:89:9: ( ( INT | FLOAT ) 'E' ( '+' | '-' )? INT )
			// DnD2D.g:89:11: ( INT | FLOAT ) 'E' ( '+' | '-' )? INT
			{
				// DnD2D.g:89:11: ( INT | FLOAT )
				int alt8 = 2;
				alt8 = this.dfa8.predict(this.input);
				switch (alt8) {
					case 1:
					// DnD2D.g:89:12: INT
					{
						this.mINT();

					}
						break;
					case 2:
					// DnD2D.g:89:18: FLOAT
					{
						this.mFLOAT();

					}
						break;

				}

				this.match('E');
				// DnD2D.g:89:29: ( '+' | '-' )?
				int alt9 = 2;
				int LA9_0 = this.input.LA(1);

				if (LA9_0 == '+' || LA9_0 == '-')
					alt9 = 1;
				switch (alt9) {
					case 1:
					// DnD2D.g:
					{
						if (this.input.LA(1) == '+' || this.input.LA(1) == '-')
							this.input.consume();
						else {
							MismatchedSetException mse = new MismatchedSetException(null, this.input);
							this.recover(mse);
							throw mse;
						}

					}
						break;

				}

				this.mINT();

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "NUMERIC"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = DnD2DLexer.STRING;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:90:8: ( '\"' (~ ( '\"' | '\\n' | '\\r' ) )* '\"' |
			// '\\'' (~ ( '\\'' | '\\n' | '\\r' ) )* '\\'' )
			int alt12 = 2;
			int LA12_0 = this.input.LA(1);

			if (LA12_0 == '\"')
				alt12 = 1;
			else if (LA12_0 == '\'')
				alt12 = 2;
			else {
				NoViableAltException nvae = new NoViableAltException("", 12, 0, this.input);

				throw nvae;
			}
			switch (alt12) {
				case 1:
				// DnD2D.g:90:10: '\"' (~ ( '\"' | '\\n' | '\\r' ) )* '\"'
				{
					this.match('\"');
					// DnD2D.g:90:14: (~ ( '\"' | '\\n' | '\\r' ) )*
					loop10: do {
						int alt10 = 2;
						int LA10_0 = this.input.LA(1);

						if (LA10_0 >= '\u0000' && LA10_0 <= '\t' || LA10_0 >= '\u000B' && LA10_0 <= '\f' || LA10_0 >= '\u000E' && LA10_0 <= '!'
								|| LA10_0 >= '#' && LA10_0 <= '\uFFFE')
							alt10 = 1;

						switch (alt10) {
							case 1:
							// DnD2D.g:90:15: ~ ( '\"' | '\\n' | '\\r' )
							{
								if (this.input.LA(1) >= '\u0000' && this.input.LA(1) <= '\t' || this.input.LA(1) >= '\u000B' && this.input.LA(1) <= '\f'
										|| this.input.LA(1) >= '\u000E' && this.input.LA(1) <= '!' || this.input.LA(1) >= '#' && this.input.LA(1) <= '\uFFFE')
									this.input.consume();
								else {
									MismatchedSetException mse = new MismatchedSetException(null, this.input);
									this.recover(mse);
									throw mse;
								}

							}
								break;

							default:
								break loop10;
						}
					} while (true);

					this.match('\"');

				}
					break;
				case 2:
				// DnD2D.g:91:4: '\\'' (~ ( '\\'' | '\\n' | '\\r' ) )* '\\''
				{
					this.match('\'');
					// DnD2D.g:91:9: (~ ( '\\'' | '\\n' | '\\r' ) )*
					loop11: do {
						int alt11 = 2;
						int LA11_0 = this.input.LA(1);

						if (LA11_0 >= '\u0000' && LA11_0 <= '\t' || LA11_0 >= '\u000B' && LA11_0 <= '\f' || LA11_0 >= '\u000E' && LA11_0 <= '&'
								|| LA11_0 >= '(' && LA11_0 <= '\uFFFE')
							alt11 = 1;

						switch (alt11) {
							case 1:
							// DnD2D.g:91:10: ~ ( '\\'' | '\\n' | '\\r' )
							{
								if (this.input.LA(1) >= '\u0000' && this.input.LA(1) <= '\t' || this.input.LA(1) >= '\u000B' && this.input.LA(1) <= '\f'
										|| this.input.LA(1) >= '\u000E' && this.input.LA(1) <= '&' || this.input.LA(1) >= '(' && this.input.LA(1) <= '\uFFFE')
									this.input.consume();
								else {
									MismatchedSetException mse = new MismatchedSetException(null, this.input);
									this.recover(mse);
									throw mse;
								}

							}
								break;

							default:
								break loop11;
						}
					} while (true);

					this.match('\'');

				}
					break;

			}
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "STRING"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = DnD2DLexer.WS;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:92:4: ( ( ' ' | '\\t' | '\\r' | '\\n' ) )
			// DnD2D.g:92:6: ( ' ' | '\\t' | '\\r' | '\\n' )
			{
				if (this.input.LA(1) >= '\t' && this.input.LA(1) <= '\n' || this.input.LA(1) == '\r' || this.input.LA(1) == ' ')
					this.input.consume();
				else {
					MismatchedSetException mse = new MismatchedSetException(null, this.input);
					this.recover(mse);
					throw mse;
				}

				this.skip();

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "WS"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = DnD2DLexer.COMMENT;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// DnD2D.g:94:5: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' |
			// '/*' ( options {greedy=false; } : . )* '*/' )
			int alt16 = 2;
			int LA16_0 = this.input.LA(1);

			if (LA16_0 == '/') {
				int LA16_1 = this.input.LA(2);

				if (LA16_1 == '/')
					alt16 = 1;
				else if (LA16_1 == '*')
					alt16 = 2;
				else {
					NoViableAltException nvae = new NoViableAltException("", 16, 1, this.input);

					throw nvae;
				}
			} else {
				NoViableAltException nvae = new NoViableAltException("", 16, 0, this.input);

				throw nvae;
			}
			switch (alt16) {
				case 1:
				// DnD2D.g:94:9: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
				{
					this.match("//");

					// DnD2D.g:94:14: (~ ( '\\n' | '\\r' ) )*
					loop13: do {
						int alt13 = 2;
						int LA13_0 = this.input.LA(1);

						if (LA13_0 >= '\u0000' && LA13_0 <= '\t' || LA13_0 >= '\u000B' && LA13_0 <= '\f' || LA13_0 >= '\u000E' && LA13_0 <= '\uFFFE')
							alt13 = 1;

						switch (alt13) {
							case 1:
							// DnD2D.g:94:14: ~ ( '\\n' | '\\r' )
							{
								if (this.input.LA(1) >= '\u0000' && this.input.LA(1) <= '\t' || this.input.LA(1) >= '\u000B' && this.input.LA(1) <= '\f'
										|| this.input.LA(1) >= '\u000E' && this.input.LA(1) <= '\uFFFE')
									this.input.consume();
								else {
									MismatchedSetException mse = new MismatchedSetException(null, this.input);
									this.recover(mse);
									throw mse;
								}

							}
								break;

							default:
								break loop13;
						}
					} while (true);

					// DnD2D.g:94:28: ( '\\r' )?
					int alt14 = 2;
					int LA14_0 = this.input.LA(1);

					if (LA14_0 == '\r')
						alt14 = 1;
					switch (alt14) {
						case 1:
						// DnD2D.g:94:28: '\\r'
						{
							this.match('\r');

						}
							break;

					}

					this.match('\n');
					this.skip();

				}
					break;
				case 2:
				// DnD2D.g:95:9: '/*' ( options {greedy=false; } : . )* '*/'
				{
					this.match("/*");

					// DnD2D.g:95:14: ( options {greedy=false; } : . )*
					loop15: do {
						int alt15 = 2;
						int LA15_0 = this.input.LA(1);

						if (LA15_0 == '*') {
							int LA15_1 = this.input.LA(2);

							if (LA15_1 == '/')
								alt15 = 2;
							else if (LA15_1 >= '\u0000' && LA15_1 <= '.' || LA15_1 >= '0' && LA15_1 <= '\uFFFE')
								alt15 = 1;

						} else if (LA15_0 >= '\u0000' && LA15_0 <= ')' || LA15_0 >= '+' && LA15_0 <= '\uFFFE')
							alt15 = 1;

						switch (alt15) {
							case 1:
							// DnD2D.g:95:42: .
							{
								this.matchAny();

							}
								break;

							default:
								break loop15;
						}
					} while (true);

					this.match("*/");

					this.skip();

				}
					break;

			}
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "COMMENT"

	@Override
	public void mTokens() throws RecognitionException {
		// DnD2D.g:1:8: ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 |
		// T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25
		// | T__26 | ID | FLOAT | INT | NUMERIC | STRING | WS | COMMENT )
		int alt17 = 23;
		alt17 = this.dfa17.predict(this.input);
		switch (alt17) {
			case 1:
			// DnD2D.g:1:10: T__11
			{
				this.mT__11();

			}
				break;
			case 2:
			// DnD2D.g:1:16: T__12
			{
				this.mT__12();

			}
				break;
			case 3:
			// DnD2D.g:1:22: T__13
			{
				this.mT__13();

			}
				break;
			case 4:
			// DnD2D.g:1:28: T__14
			{
				this.mT__14();

			}
				break;
			case 5:
			// DnD2D.g:1:34: T__15
			{
				this.mT__15();

			}
				break;
			case 6:
			// DnD2D.g:1:40: T__16
			{
				this.mT__16();

			}
				break;
			case 7:
			// DnD2D.g:1:46: T__17
			{
				this.mT__17();

			}
				break;
			case 8:
			// DnD2D.g:1:52: T__18
			{
				this.mT__18();

			}
				break;
			case 9:
			// DnD2D.g:1:58: T__19
			{
				this.mT__19();

			}
				break;
			case 10:
			// DnD2D.g:1:64: T__20
			{
				this.mT__20();

			}
				break;
			case 11:
			// DnD2D.g:1:70: T__21
			{
				this.mT__21();

			}
				break;
			case 12:
			// DnD2D.g:1:76: T__22
			{
				this.mT__22();

			}
				break;
			case 13:
			// DnD2D.g:1:82: T__23
			{
				this.mT__23();

			}
				break;
			case 14:
			// DnD2D.g:1:88: T__24
			{
				this.mT__24();

			}
				break;
			case 15:
			// DnD2D.g:1:94: T__25
			{
				this.mT__25();

			}
				break;
			case 16:
			// DnD2D.g:1:100: T__26
			{
				this.mT__26();

			}
				break;
			case 17:
			// DnD2D.g:1:106: ID
			{
				this.mID();

			}
				break;
			case 18:
			// DnD2D.g:1:109: FLOAT
			{
				this.mFLOAT();

			}
				break;
			case 19:
			// DnD2D.g:1:115: INT
			{
				this.mINT();

			}
				break;
			case 20:
			// DnD2D.g:1:119: NUMERIC
			{
				this.mNUMERIC();

			}
				break;
			case 21:
			// DnD2D.g:1:127: STRING
			{
				this.mSTRING();

			}
				break;
			case 22:
			// DnD2D.g:1:134: WS
			{
				this.mWS();

			}
				break;
			case 23:
			// DnD2D.g:1:137: COMMENT
			{
				this.mCOMMENT();

			}
				break;

		}

	}

	protected DFA8 dfa8 = new DFA8(this);
	protected DFA17 dfa17 = new DFA17(this);
	static final String DFA8_eotS = "\4\uffff";
	static final String DFA8_eofS = "\4\uffff";
	static final String DFA8_minS = "\1\60\1\56\2\uffff";
	static final String DFA8_maxS = "\1\71\1\105\2\uffff";
	static final String DFA8_acceptS = "\2\uffff\1\1\1\2";
	static final String DFA8_specialS = "\4\uffff}>";
	static final String[] DFA8_transitionS = { "\12\1", "\1\3\1\uffff\12\1\13\uffff\1\2", "", "" };

	static final short[] DFA8_eot = DFA.unpackEncodedString(DnD2DLexer.DFA8_eotS);
	static final short[] DFA8_eof = DFA.unpackEncodedString(DnD2DLexer.DFA8_eofS);
	static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DnD2DLexer.DFA8_minS);
	static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DnD2DLexer.DFA8_maxS);
	static final short[] DFA8_accept = DFA.unpackEncodedString(DnD2DLexer.DFA8_acceptS);
	static final short[] DFA8_special = DFA.unpackEncodedString(DnD2DLexer.DFA8_specialS);
	static final short[][] DFA8_transition;

	static {
		int numStates = DnD2DLexer.DFA8_transitionS.length;
		DFA8_transition = new short[numStates][];
		for (int i = 0; i < numStates; i++)
			DnD2DLexer.DFA8_transition[i] = DFA.unpackEncodedString(DnD2DLexer.DFA8_transitionS[i]);
	}

	class DFA8 extends DFA {

		public DFA8(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 8;
			this.eot = DnD2DLexer.DFA8_eot;
			this.eof = DnD2DLexer.DFA8_eof;
			this.min = DnD2DLexer.DFA8_min;
			this.max = DnD2DLexer.DFA8_max;
			this.accept = DnD2DLexer.DFA8_accept;
			this.special = DnD2DLexer.DFA8_special;
			this.transition = DnD2DLexer.DFA8_transition;
		}

		@Override
		public String getDescription() {
			return "89:11: ( INT | FLOAT )";
		}
	}

	static final String DFA17_eotS = "\1\uffff\1\24\7\uffff\1\26\3\15\1\uffff\1\34\10\uffff\1\15\1\37"
			+ "\2\15\3\uffff\1\15\1\uffff\1\15\1\45\1\46\1\47\1\50\4\uffff";
	static final String DFA17_eofS = "\51\uffff";
	static final String DFA17_minS = "\1\11\1\55\7\uffff\1\75\1\111\1\123\1\117\1\uffff\1\56\10\uffff"
			+ "\1\113\1\43\1\114\1\124\1\60\2\uffff\1\105\1\uffff\1\114\1\43\1" + "\60\2\43\4\uffff";
	static final String DFA17_maxS = "\1\172\1\75\7\uffff\1\75\1\111\1\123\1\125\1\uffff\1\105\10\uffff"
			+ "\1\113\1\172\1\114\1\124\1\71\2\uffff\1\105\1\uffff\1\114\1\172" + "\1\105\2\172\4\uffff";
	static final String DFA17_acceptS = "\2\uffff\1\2\1\3\1\4\1\5\1\6\1\7\1\10\4\uffff\1\21\1\uffff\1\25"
			+ "\1\26\1\27\1\1\1\14\1\11\1\13\1\12\5\uffff\1\23\1\24\1\uffff\1\16" + "\5\uffff\1\20\1\22\1\15\1\17";
	static final String DFA17_specialS = "\51\uffff}>";
	static final String[] DFA17_transitionS = {
			"\2\20\2\uffff\1\20\22\uffff\1\20\1\10\1\17\1\uffff\1\15\2\uffff" + "\1\17\1\5\1\6\2\uffff\1\4\2\uffff\1\21\12\16\1\2\1\uffff\1\1"
					+ "\1\7\1\11\1\15\1\uffff\10\15\1\13\2\15\1\12\1\15\1\14\14\15" + "\3\uffff\1\3\34\15", "\1\22\17\uffff\1\23", "", "", "", "", "", "", "",
			"\1\25", "\1\27", "\1\30", "\1\32\5\uffff\1\31", "", "\1\33\1\uffff\12\16\13\uffff\1\35", "", "", "", "", "", "", "", "", "\1\36",
			"\1\15\11\uffff\1\15\2\uffff\12\15\7\uffff\32\15\4\uffff\1" + "\15\1\uffff\32\15", "\1\40", "\1\41", "\12\42", "", "", "\1\43", "", "\1\44",
			"\1\15\11\uffff\1\15\2\uffff\12\15\7\uffff\32\15\4\uffff\1" + "\15\1\uffff\32\15", "\12\42\13\uffff\1\35",
			"\1\15\11\uffff\1\15\2\uffff\12\15\7\uffff\32\15\4\uffff\1" + "\15\1\uffff\32\15",
			"\1\15\11\uffff\1\15\2\uffff\12\15\7\uffff\32\15\4\uffff\1" + "\15\1\uffff\32\15", "", "", "", "" };

	static final short[] DFA17_eot = DFA.unpackEncodedString(DnD2DLexer.DFA17_eotS);
	static final short[] DFA17_eof = DFA.unpackEncodedString(DnD2DLexer.DFA17_eofS);
	static final char[] DFA17_min = DFA.unpackEncodedStringToUnsignedChars(DnD2DLexer.DFA17_minS);
	static final char[] DFA17_max = DFA.unpackEncodedStringToUnsignedChars(DnD2DLexer.DFA17_maxS);
	static final short[] DFA17_accept = DFA.unpackEncodedString(DnD2DLexer.DFA17_acceptS);
	static final short[] DFA17_special = DFA.unpackEncodedString(DnD2DLexer.DFA17_specialS);
	static final short[][] DFA17_transition;

	static {
		int numStates = DnD2DLexer.DFA17_transitionS.length;
		DFA17_transition = new short[numStates][];
		for (int i = 0; i < numStates; i++)
			DnD2DLexer.DFA17_transition[i] = DFA.unpackEncodedString(DnD2DLexer.DFA17_transitionS[i]);
	}

	class DFA17 extends DFA {

		public DFA17(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 17;
			this.eot = DnD2DLexer.DFA17_eot;
			this.eof = DnD2DLexer.DFA17_eof;
			this.min = DnD2DLexer.DFA17_min;
			this.max = DnD2DLexer.DFA17_max;
			this.accept = DnD2DLexer.DFA17_accept;
			this.special = DnD2DLexer.DFA17_special;
			this.transition = DnD2DLexer.DFA17_transition;
		}

		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | ID | FLOAT | INT | NUMERIC | STRING | WS | COMMENT );";
		}
	}

}
