package es.upm.fi.dia.oeg.kyrie.rewriter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;


public class Clause {
	private static class AtomIndexNode {
		private final Term m_literal;
		private final AtomIndexNode m_next;
		private boolean m_matched;

		public AtomIndexNode(Term literal, AtomIndexNode next) {
			this.m_literal = literal;
			this.m_next = next;
			this.m_matched = false;
		}
	}

	public static final Map<Variable, Term> EMPTY_SUBSTITUTION = Collections.emptyMap();
	private final Term[] m_body;
	private final Term m_head;
	public final String m_canonicalRepresentation;

	public boolean toBeIgnored = false;
	public boolean[] m_selectedBody;
	public boolean m_selectedHead;

	public char m_type;

	public Clause(Term[] body, Term head) {
		this.m_body = body;
		this.m_head = head;
		this.m_selectedBody = new boolean[this.m_body.length];
		this.m_selectedHead = false;
		this.m_canonicalRepresentation = this.computeCanonicalRepresentation();
		this.m_type = '\0';
	}

	private Term applySubstitution(Term term, Map<Variable, Term> substitution) {
		if (term instanceof Variable) {
			Term replacement = substitution.get(term);
			if (replacement != null)
				return replacement;
		}
		return term;
	}

	private Map<String, AtomIndexNode> buildAtomIndex(Term[] atoms) {
		Map<String, AtomIndexNode> atomIndex = new HashMap<String, AtomIndexNode>();
		for (Term atom : atoms)
			atomIndex.put(atom.getName(), new AtomIndexNode(atom, atomIndex.get(atom.getName())));
		return atomIndex;
	}

	/**
	 * Performs basic preliminary tests in order to check whether this can be equivalent to that up to variable renaming
	 * 
	 * @param that
	 * @return
	 */
	private boolean canbeEquivalent(Clause that) {
		if (this.getHead().getName().equals(that.getHead().getName()) && this.getHead().getArity() == that.getHead().getArity()
				&& this.getBody().length == that.getBody().length && this.hasSameBodyAtoms(that))
			return true;
		return false;
	}

	/**
	 * Performs basic preliminary tests in order to check whether this can subsume a given clause
	 * 
	 * @param that
	 * @return
	 */
	private boolean canSubsume(Clause that) {
		if (this.getHead().getName().equals(that.getHead().getName()) && this.getHead().getArity() == that.getHead().getArity()
				&& this.hasSubsetOfBodyAtoms(that))
			return true;
		return false;
	}

	private String computeCanonicalRepresentation() {
		Arrays.sort(this.m_body, new Comparator<Term>() {
			public int compare(Term t1, Term t2) {
				return ((FunctionalTerm) t1).getFunctionalPrefix().compareTo(((FunctionalTerm) t2).getFunctionalPrefix());
			}
		});
		return this.toString();
	}

	public boolean containsBinaryBodyAtoms() {
		for (Term t : this.m_body)
			if (t.getArity() == 2)
				return true;
		return false;
	}

	public boolean containsEquality() {
		if (this.m_head.getName().equals("="))
			return true;
		for (Term t : this.m_body)
			if (t.getName().equals("="))
				return true;
		return false;
	}

	public boolean containsFunctionalTerms() {
		if (this.m_head.getDepth() - 1 > 0)
			return true;
		for (Term element : this.m_body)
			if (element.getDepth() - 1 > 0)
				return true;
		return false;
	}

	public boolean containsNominalPredicate() {
		if (this.m_head.getName().equals("$"))
			return true;
		for (Term t : this.m_body)
			if (t.getName().equals("$"))
				return true;
		return false;
	}

	public boolean containsUnmappedAtoms(Collection<String> mappings) {
		for (Term t : this.m_body)
			if (!mappings.contains(t.getName()))
				return true;
		return !(this.m_head.getName().equals("Q") || mappings.contains(this.m_head.getName()));
	}

	public boolean containsUnmappedAtomsinBody(Collection<String> mappings) {
		for (Term t : this.m_body)
			if (!mappings.contains(t.getName()))
				return true;
		return false;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Clause))
			return false;
		Clause c = (Clause) o;
		return c.isEquivalentUpToVariableRenaming(this) && this.isEquivalentUpToVariableRenaming(c);
	}

	public Term[] getBody() {
		return this.m_body;
	}

	private Map<String, AtomIndexNode> getBodyAtomIndex() {
		return this.buildAtomIndex(this.m_body);
	}

	public Term getHead() {
		return this.m_head;
	}

	private Map<String, AtomIndexNode> getHeadAtomIndex() {
		return this.buildAtomIndex(new Term[] { this.m_head });
	}

	/**
	 * Returns an ArrayList<Variable> containing all the variables occurring in the clause (no repetitions) as they occur
	 * 
	 * @return
	 */
	public ArrayList<Variable> getVariables() {
		ArrayList<Variable> result = new ArrayList<Variable>();

		// Get the variables of the head
		for (int i = 0; i < this.m_head.getArguments().length; i++) {
			Term t = this.m_head.getArguments()[i].getVariableOrConstant();
			if (t instanceof Variable && !result.contains(t))
				result.add((Variable) t);
		}

		// Get the variables of the body
		for (Term element : this.m_body)
			for (int i = 0; i < element.getArguments().length; i++) {
				Term t = element.getArguments()[i].getVariableOrConstant();
				if (t instanceof Variable && !result.contains(t))
					result.add((Variable) t);
			}

		return result;
	}

	public boolean hasGroundBody() {
		for (Term element : this.m_body)
			for (int j = 0; j < element.getArity(); j++)
				if (element.getArgument(j).getVariableOrConstant() instanceof Variable
						|| element.getArgument(j).getVariableOrConstant() instanceof FunctionalTerm && element.getArgument(j).getDepth() > 0)
					return false;
		return true;
	}

	// @Override
	// public boolean equals(Object o){
	// if(!(o instanceof Clause))
	// return false;
	// Clause c = (Clause) o;
	// List<Term> cBody = Arrays.asList(c.getBody());
	// List<Term> thisBody = Arrays.asList(this.getBody());
	// if (!(cBody.containsAll(thisBody) && thisBody.containsAll(cBody)))
	// return false;
	// return this.getHead().equals(c.getHead());
	// }

	public boolean hasGroundHead() {
		for (int j = 0; j < this.m_head.getArity(); j++)
			if (this.m_head.getArgument(j).getVariableOrConstant() instanceof Variable
					|| this.m_head.getArgument(j).getVariableOrConstant() instanceof FunctionalTerm && this.m_head.getArgument(j).getDepth() > 0)
				return false;
		return true;
	}

	/**
	 * Decides if the body atoms of this are the same as the body atoms of that
	 * 
	 * @param that
	 * @return
	 */
	private boolean hasSameBodyAtoms(Clause that) {

		for (int i = 0; i < this.m_body.length; i++)
			if (!(this.m_body[i].getArity() == that.m_body[i].getArity() && this.m_body[i].getName().equals(that.m_body[i].getName())))
				return false;
		return true;
	}

	/**
	 * Decides if the set of body atoms of this is a subset of the set of body atoms of that
	 * 
	 * @param that
	 * @return
	 */
	private boolean hasSubsetOfBodyAtoms(Clause that) {
		HashSet<String> superset = new HashSet<String>();

		for (Term element : that.m_body)
			superset.add(element.getName());

		for (int i = 0; i < this.m_body.length; i++)
			if (!superset.contains(this.m_body[i].getName()))
				return false;

		return true;
	}

	/**
	 * Decides if this is equivalent to that up to variable renaming
	 * 
	 * @param that
	 * @return
	 */
	public boolean isEquivalentUpToVariableRenaming(Clause rule) {
		if (!this.canbeEquivalent(rule))
			return false;

		Clause ruleCore = rule;
		Term[][] subsumingAtoms = new Term[][] { new Term[] { this.m_head }, this.m_body };
		Map<String, AtomIndexNode> subsumedHeadAtomsIndex = ruleCore.getHeadAtomIndex();
		Map<String, AtomIndexNode> subsumedBodyAtomsIndex = ruleCore.getBodyAtomIndex();
		if (subsumedHeadAtomsIndex == null || subsumedBodyAtomsIndex == null)
			return false;
		else
			return this.match(true, subsumingAtoms, subsumedHeadAtomsIndex, subsumedBodyAtomsIndex, Clause.EMPTY_SUBSTITUTION, 0, 0);
	}

	public boolean isGround() {
		return this.hasGroundHead() && this.hasGroundBody();
	}

	public boolean isQueryClause() {
		if (this.m_head.getName().equals("Q"))
			return true;
		return false;
	}

	public boolean isTautology() {
		for (Term element : this.m_body)
			if (element.toString().equals(this.m_head.toString()))
				return true;
		return false;
	}

	private boolean match(boolean clauseSubsumption, Term[][] subsumingAtoms, Map<String, AtomIndexNode> subsumedHeadAtomsIndex,
			Map<String, AtomIndexNode> subsumedBodyAtomsIndex, Map<Variable, Term> substitution, int headBodyIndex, int matchAtomIndex) {
		Term[] activeSubsumingAtoms = subsumingAtoms[headBodyIndex];
		if (matchAtomIndex == activeSubsumingAtoms.length) {
			if (headBodyIndex == 0)
				return this.match(clauseSubsumption, subsumingAtoms, subsumedHeadAtomsIndex, subsumedBodyAtomsIndex, substitution, 1, 0);
			return true;

		}
		Term subsumingFormula = activeSubsumingAtoms[matchAtomIndex];
		if (!(subsumingFormula instanceof Term))
			return false;
		Term subsumingAtom = subsumingFormula;
		Map<String, AtomIndexNode> subsumedAtomsIndex;
		if (headBodyIndex == 0)
			subsumedAtomsIndex = subsumedHeadAtomsIndex;
		else
			subsumedAtomsIndex = subsumedBodyAtomsIndex;
		String subsumingAtomPredicate = subsumingAtom.getName();
		AtomIndexNode candidates = subsumedAtomsIndex.get(subsumingAtomPredicate);
		while (candidates != null)
			if (clauseSubsumption) {
				if (!candidates.m_matched) {
					Map<Variable, Term> matchedSubstitution = this.matchAtoms(subsumingAtom, candidates.m_literal, substitution);
					if (matchedSubstitution != null) {
						candidates.m_matched = true;
						boolean result = this.match(clauseSubsumption, subsumingAtoms, subsumedHeadAtomsIndex, subsumedBodyAtomsIndex, matchedSubstitution,
								headBodyIndex, matchAtomIndex + 1);
						if (result)
							return result;
						candidates.m_matched = false;
					}
				}
				candidates = candidates.m_next;
			} else {
				// Query subsumption
				Map<Variable, Term> matchedSubstitution = this.matchAtoms(subsumingAtom, candidates.m_literal, substitution);
				if (matchedSubstitution != null) {
					boolean result = this.match(clauseSubsumption, subsumingAtoms, subsumedHeadAtomsIndex, subsumedBodyAtomsIndex, matchedSubstitution,
							headBodyIndex, matchAtomIndex + 1);
					if (result)
						return result;
				}
				candidates = candidates.m_next;
			}
		return false;

	}

	private Map<Variable, Term> matchAtoms(Term subsumingAtom, Term subsumedAtom, Map<Variable, Term> substitution) {
		boolean substitutionCopied = false;
		for (int argumentIndex = 0; argumentIndex < subsumingAtom.getArity(); argumentIndex++) {
			Term subsumingArgument = subsumingAtom.getArgument(argumentIndex);
			Term subsumedArgument = subsumedAtom.getArgument(argumentIndex);
			if (subsumingArgument instanceof Variable) {
				Term existingBinding = substitution.get(subsumingArgument);
				if (existingBinding == null) {
					if (!substitutionCopied) {
						substitution = new HashMap<Variable, Term>(substitution);
						substitutionCopied = true;
					}
					substitution.put((Variable) subsumingArgument, subsumedArgument);
				}
			}
			Term subsumingArgumentSubstitutionApplied = this.applySubstitution(subsumingAtom.getArgument(argumentIndex), substitution);
			if (!subsumingArgumentSubstitutionApplied.equals(subsumedArgument))
				return null;
		}
		return substitution;
	}

	/**
	 * Renames the variables of the clause via mapping
	 * 
	 * @param termFactory
	 * @param mapping
	 * @return
	 */
	public Clause renameVariables(TermFactory termFactory, HashMap<Variable, Integer> mapping) {
		Term headRenamed = this.m_head.renameVariables(termFactory, mapping);

		Term[] bodyRenamed = new Term[this.m_body.length];
		for (int index = 0; index < this.m_body.length; index++)
			bodyRenamed[index] = this.m_body[index].renameVariables(termFactory, mapping);

		Clause clauseRenamed = new Clause(bodyRenamed, headRenamed);
		clauseRenamed.setSelectedBody(this.m_selectedBody);
		clauseRenamed.setSelectedHead(this.m_selectedHead);

		return clauseRenamed;
	}

	/**
	 * Renames the variables of the clause via offset
	 * 
	 * @param termFactory
	 * @param offset
	 * @return
	 */
	public Clause renameVariables(TermFactory termFactory, int offset) {

		Term headRenamed = this.m_head.offsetVariables(termFactory, offset);

		Term[] bodyRenamed = new Term[this.m_body.length];
		for (int index = 0; index < this.m_body.length; index++)
			bodyRenamed[index] = this.m_body[index].offsetVariables(termFactory, offset);

		Clause clauseRenamed = new Clause(bodyRenamed, headRenamed);
		clauseRenamed.setSelectedBody(this.m_selectedBody);
		clauseRenamed.setSelectedHead(this.m_selectedHead);

		return clauseRenamed;
	}

	public void setSelectedBody(boolean[] selectedBody) {
		this.m_selectedBody = selectedBody;
	}

	public void setSelectedHead(boolean selectedHead) {
		this.m_selectedHead = selectedHead;
	}

	/**
	 * Decides if this subsumes that
	 * 
	 * @param that
	 * @return
	 */
	public boolean subsumes(Clause rule, boolean clauseSubsumption) {
		if (!this.canSubsume(rule))
			return false;

		Clause ruleCore = rule;
		Term[][] subsumingAtoms = new Term[][] { new Term[] { this.m_head }, this.m_body };
		Map<String, AtomIndexNode> subsumedHeadAtomsIndex = ruleCore.getHeadAtomIndex();
		Map<String, AtomIndexNode> subsumedBodyAtomsIndex = ruleCore.getBodyAtomIndex();
		if (subsumedHeadAtomsIndex == null || subsumedBodyAtomsIndex == null)
			return false;
		return this.match(clauseSubsumption, subsumingAtoms, subsumedHeadAtomsIndex, subsumedBodyAtomsIndex, Clause.EMPTY_SUBSTITUTION, 0, 0);
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		this.toString(buffer);
		return buffer.toString();
	}

	private void toString(StringBuffer buffer) {
		this.m_head.toString(buffer);

		if (this.m_body.length > 0) {
			buffer.append("  <-  ");
			for (int index = 0; index < this.m_body.length; index++) {
				if (index != 0)
					buffer.append(", ");
				this.m_body[index].toString(buffer);
			}
		}
	}
}
