package es.upm.fi.dia.oeg.kyrie;

import java.util.ArrayList;
import java.util.HashMap;
import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.sparql.core.Var;


/**
 * 
 */

class Correspondences {

	private HashMap<String, LiteralDetails> lds = new HashMap<String, LiteralDetails>();
	private ArrayList<Var> variables;

	public void addDetails(String literal, String lang, RDFDatatype dtype) {
		this.lds.put(literal, new LiteralDetails(lang, dtype));
	}

	public LiteralDetails getDetails(String k) {
		return this.lds.get(k);
	}

	public Correspondences() {
		super();
		this.lds = new HashMap<String, LiteralDetails>();
		this.variables = new ArrayList<Var>();
	}

	public class LiteralDetails {
		public String lang;
		public RDFDatatype dtype;

		public LiteralDetails(String lang, RDFDatatype dtype) {
			this.lang = lang;
			this.dtype = dtype;
		}
	}

	public boolean isLiteral(String s) {
		return this.lds.containsKey(s);
	}

	public void add(Var v) {
		this.variables.add(v);
	}
}
