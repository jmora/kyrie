package es.upm.fi.dia.oeg.kyrie;

/*
 (c) Copyright 2002, 2003, 2004, 2005 Hewlett-Packard Development Company, LP
 [See end of file]
 $Id: Node.java,v 1.5 2005/07/04 14:48:09 jeremy_carroll Exp $
 */
//modified by jmora@fi.upm.es, actually extracted from an old piece of code because I need this.

import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.datatypes.TypeMapper;
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.impl.LiteralLabel;
import com.hp.hpl.jena.graph.impl.LiteralLabelFactory;
import com.hp.hpl.jena.rdf.model.AnonId;
import com.hp.hpl.jena.shared.JenaException;
import com.hp.hpl.jena.shared.PrefixMapping;

public class MoreJena {

	public static Node create(PrefixMapping pm, String x) {
		if (x.equals(""))
			throw new JenaException("Node.create does not accept an empty string as argument");
		char first = x.charAt(0);
		if (first == '\'' || first == '\"')
			return Node.createLiteral(MoreJena.newString(pm, first, x));
		if (Character.isDigit(first))
			return Node.createLiteral(LiteralLabelFactory.create(x, "", XSDDatatype.XSDinteger));
		if (first == '_')
			return Node.createAnon(new AnonId(x));
		if (x.equals("??"))
			return Node.ANY;
		if (first == '?')
			return Node.createVariable(x.substring(1));
		if (first == '&')
			return Node.createURI("q:" + x.substring(1));
		int colon = x.indexOf(':');
		String d = pm.getNsPrefixURI("");
		return colon < 0 ? Node.createURI((d == null ? "eh:/" : d) + x) : Node.createURI(pm.expandPrefix(x));
	}

	private static LiteralLabel newString(PrefixMapping pm, char quote, String nodeString) {
		int close = nodeString.lastIndexOf(quote);
		return MoreJena.literal(pm, nodeString.substring(1, close), nodeString.substring(close + 1));
	}

	private static LiteralLabel literal(PrefixMapping pm, String spelling, String langOrType) {
		String content = MoreJena.unEscape(spelling);
		int colon = langOrType.indexOf(':');
		return colon < 0 ? LiteralLabelFactory.create(content, langOrType, false) : LiteralLabelFactory.create(content, "", MoreJena.getType(pm.expandPrefix(langOrType)));
	}

	private static RDFDatatype getType(String s) {
		return TypeMapper.getInstance().getSafeTypeByName(s);
	}

	private static String unEscape(String spelling) {
		if (spelling.indexOf('\\') < 0)
			return spelling;
		StringBuffer result = new StringBuffer(spelling.length());
		int start = 0;
		while (true) {
			int b = spelling.indexOf('\\', start);
			if (b < 0)
				break;
			result.append(spelling.substring(start, b));
			result.append(MoreJena.unEscape(spelling.charAt(b + 1)));
			start = b + 2;
		}
		result.append(spelling.substring(start));
		return result.toString();
	}

	private static char unEscape(char ch) {
		switch (ch) {
		case '\\':
		case '\"':
		case '\'':
			return ch;
		case 'n':
			return '\n';
		case 's':
			return ' ';
		case 't':
			return '\t';
		default:
			return 'Z';
		}
	}

}

/*
 * (c) Copyright 2005 Hewlett-Packard Development Company, LP All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 1. Redistributions of source code must retain the
 * above copyright notice, this list of conditions and the following disclaimer. 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the distribution. 3. The name of the author may not be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
