package es.upm.fi.dia.oeg.kyrie.rewriter;

import java.util.ArrayList;
import java.util.Collection;

import es.upm.fi.dia.oeg.kyrie.optimizer.Optimizer;

public class Saturator {
	private TermFactory m_termFactory;
	protected final Resolution m_resolution;
	protected ArrayList<String> m_clausesCanonicals;
	protected ArrayList<Clause> m_workedOffClauses, m_unprocessedClauses;
	private Optimizer mOptimizer;

	public Saturator(TermFactory termFactory, Optimizer mOptimizer) {
		this.m_termFactory = termFactory;
		this.m_workedOffClauses = new ArrayList<Clause>();
		this.m_unprocessedClauses = new ArrayList<Clause>();
		this.m_clausesCanonicals = new ArrayList<String>();
		this.mOptimizer = mOptimizer;
		this.m_resolution = new Resolution(this);
	}

	public TermFactory getTermFactory() {
		return this.m_termFactory;
	}

	// I should not be doing this, I don't really remember how it was and I don't have the time to check it...
	public ArrayList<Clause> inclusiveUnfold(ArrayList<Clause> clauses, Collection<String> recursivePredicates) {
		if (!recursivePredicates.contains("Q"))
			recursivePredicates.add("Q");
		ArrayList<Clause> query = new ArrayList<Clause>();
		ArrayList<Clause> ontology = new ArrayList<Clause>();
		// separating query and ontology (again)
		for (Clause c : clauses)
			if (!c.isTautology())
				if (recursivePredicates.contains(c.getHead().getName()))
					query.add(c);
				else
					ontology.add(c);
		query = this.saturate(ontology, query, new SelectionFunctionSaturateExclude(recursivePredicates), false);
		return query;
	}

	/**
	 * Prunes the set of workedOffClauses based on a given selection function
	 * 
	 * @param selectionFunction
	 * @return
	 */
	private ArrayList<Clause> prune(SelectionFunction selectionFunction) {
		ArrayList<Clause> result = new ArrayList<Clause>();
		this.m_clausesCanonicals.clear();

		for (Clause c : this.m_workedOffClauses)
			if (!selectionFunction.isToBePruned(c)) {
				result.add(c);
				this.m_clausesCanonicals.add(c.m_canonicalRepresentation);
			}
		return result;
	}

	public ArrayList<Clause> saturate(ArrayList<Clause> clauses, SelectionFunction selectionFunction) {
		return this.saturate(clauses, new ArrayList<Clause>(), selectionFunction, true);
	}

	public ArrayList<Clause> saturate(ArrayList<Clause> clauses, ArrayList<Clause> query, SelectionFunction selectionFunction, boolean toPrune) {
		this.m_clausesCanonicals = new ArrayList<String>();
		for (Clause c : clauses)
			selectionFunction.selectAtoms(c);
		ResolutionManager rm;
		boolean separatedMode = !query.isEmpty();
		if (separatedMode) {
			for (Clause c : query)
				this.m_clausesCanonicals.add(c.m_canonicalRepresentation);
			rm = new ResolutionManager(query, this.mOptimizer, selectionFunction);
		} else {
			for (Clause c : clauses)
				this.m_clausesCanonicals.add(c.m_canonicalRepresentation);
			rm = new ResolutionManager(clauses, this.mOptimizer, selectionFunction);
		}
		ArrayList<Clause> results;
		while (!rm.isPendingEmpty()) {
			Clause givenClause = rm.removePending();
			rm.addWorked(givenClause);
			selectionFunction.selectAtoms(givenClause);
			if (separatedMode)
				results = this.m_resolution.generateResolvents(givenClause, clauses);
			else
				results = this.m_resolution.generateResolvents(givenClause, rm.getAllWorked());
			for (Clause resolvent : results)
				if (!resolvent.isTautology() && !this.m_clausesCanonicals.contains(resolvent.m_canonicalRepresentation)) {
					this.m_clausesCanonicals.add(resolvent.m_canonicalRepresentation);
					rm.addPending(resolvent);
				}
		}
		if (!separatedMode) {
			this.m_workedOffClauses = rm.getAllWorked();
			return this.prune(selectionFunction);
		}
		if (toPrune) {
			this.m_workedOffClauses = rm.getAllWorked();
			this.m_workedOffClauses.addAll(clauses);
			return this.prune(selectionFunction);
		}
		return rm.getAllWorked();
	}

}
