package es.upm.fi.dia.oeg.kyrie.rewriter;

import java.util.ArrayList;
import java.util.List;


public final class TermFactory {
	protected final List<Variable> m_variables;
	protected Entry[] m_entries;
	protected int m_numberOfTerms;
	protected int m_resizeThreshold;

	public TermFactory() {
		this.m_variables = new ArrayList<Variable>(1000);
		this.m_entries = new Entry[16];
		this.m_resizeThreshold = (int) (this.m_entries.length * 0.75);
	}

	public Variable getVariable(int index) {
		while (index >= this.m_variables.size())
			this.m_variables.add(null);
		Variable variable = this.m_variables.get(index);
		if (variable == null) {
			variable = new Variable(index);
			this.m_variables.set(index, variable);
		}
		return variable;
	}

	public FunctionalTerm getConstant(String name) {
		return this.getFunctionalTerm(name, Term.NO_ARGUMENTS);
	}

	public FunctionalTerm getFunctionalTerm(String name, Term... arguments) {
		int hashCode = TermFactory.interningHashCode(name, arguments);
		int index = hashCode & this.m_entries.length - 1;
		Entry entry = this.m_entries[index];
		while (entry != null) {
			if (hashCode == entry.m_hashCode && entry.equals(name, arguments))
				return entry.m_term;
			entry = entry.m_next;
		}
		FunctionalTerm functionalTerm = new FunctionalTerm(name, arguments);
		this.m_entries[index] = new Entry(functionalTerm, this.m_entries[index]);
		this.m_numberOfTerms++;
		if (this.m_numberOfTerms >= this.m_resizeThreshold)
			this.resizeEntries();
		return functionalTerm;
	}

	protected void resizeEntries() {
		Entry[] newEntries = new Entry[this.m_entries.length * 2];
		for (int index = this.m_entries.length - 1; index >= 0; --index) {
			Entry entry = this.m_entries[index];
			while (entry != null) {
				Entry nextEntry = entry.m_next;
				int newIndex = entry.m_hashCode & newEntries.length - 1;
				entry.m_next = newEntries[newIndex];
				newEntries[newIndex] = entry;
				entry = nextEntry;
			}
		}
		this.m_entries = newEntries;
		this.m_resizeThreshold = (int) (this.m_entries.length * 0.75);
	}

	protected static int interningHashCode(String name, Term[] arguments) {
		int hashCode = name.hashCode();
		for (Term argument : arguments)
			hashCode += argument.hashCode();
		return hashCode;
	}

	protected static final class Entry {
		protected final FunctionalTerm m_term;
		protected final int m_hashCode;
		protected Entry m_next;

		public Entry(FunctionalTerm term, Entry next) {
			this.m_term = term;
			this.m_hashCode = TermFactory.interningHashCode(this.m_term.m_name, this.m_term.m_arguments);
			this.m_next = next;
		}

		public boolean equals(String name, Term[] arguments) {
			if (!name.equals(this.m_term.m_name))
				return false;
			if (arguments.length != this.m_term.m_arguments.length)
				return false;
			for (int index = arguments.length - 1; index >= 0; --index)
				if (arguments[index] != this.m_term.m_arguments[index])
					return false;
			return true;
		}
	}
}
