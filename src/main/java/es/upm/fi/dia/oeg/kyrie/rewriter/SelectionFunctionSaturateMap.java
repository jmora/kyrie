// Copyright 2008 by Oxford University; see license.txt for details

package es.upm.fi.dia.oeg.kyrie.rewriter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


public class SelectionFunctionSaturateMap extends SelectionFunction {

	private Set<String> mappings;

	public SelectionFunctionSaturateMap(Collection<String> mappings) {
		this.mappings = new HashSet<String>(mappings);
	}

	@Override
	public boolean isToBePruned(Clause c) {
		return c.containsUnmappedAtoms(this.mappings);
	}

	@Override
	public void selectAtoms(Clause c) {

		// Initialize
		c.m_selectedHead = true;
		c.m_selectedBody = new boolean[c.getBody().length];

		for (int i = 0; i < c.getBody().length; i++)
			if (!this.mappings.contains(c.getBody()[i].getName())) {
				c.m_selectedBody[i] = true;
				c.m_selectedHead = false;
			}
		if (this.mappings.contains(c.getHead().getName()))
			c.m_selectedHead = false;
	}
}
