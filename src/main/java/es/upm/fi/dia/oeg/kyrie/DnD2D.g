grammar ISI2RQR;


@header {
package es.upm.fi.dia.oeg.newrqr;
import java.util.ArrayList;
import org.oxford.comlab.requiem.rewriter.TermFactory;
import org.oxford.comlab.requiem.rewriter.Clause;
import org.oxford.comlab.requiem.rewriter.Term;
import org.oxford.comlab.requiem.rewriter.FunctionalTerm;
}

@lexer::header {
package es.upm.fi.dia.oeg.newrqr;
}

@members {
public ArrayList<String> varNames = new ArrayList<String>();
TermFactory tf = new TermFactory();
boolean queryFound = false;
}

program returns [ArrayList<Clause> program]
@init {$program = new ArrayList<Clause>();}
  : (rule {$program.add($rule.clause);})* EOF ;

rule returns [Clause clause]
	:	relation_predicate ('<-'|':-') rule_body { $clause = new Clause($rule_body.body.toArray(new Term[0]), $relation_predicate.t); } ;
	
rule_body returns [ArrayList<Term> body]
@init {$body = new ArrayList<Term>();}
	:	p1=predicate {$body.add($p1.t);}
    (('^'|',') p2=predicate {$body.add($p2.t);})* 
  ;

predicate returns [Term t]
	:	relation_predicate  {$t = $relation_predicate.t;}
  | builtin_predicate   {$t = $builtin_predicate.t;}
  ;
	
relation_predicate returns [Term t]
@init {ArrayList<Term> body = new ArrayList<Term>();}
	:	predicateName '(' c1=column_value {body.add($c1.t);} (',' c2=column_value {body.add($c2.t);})* ')'
    {$t = new FunctionalTerm($predicateName.text, body.toArray(new Term[0]));}
  ;

builtin_predicate returns [Term t]
	:	'(' v1=column_value comparison v2=column_value? ')' {
      if (v2==null)
        $t = tf.getFunctionalTerm($comparison.text, $v1.t);
      else
        $t = tf.getFunctionalTerm($comparison.text, $v1.t, $v2.t);
      };
	
column_value returns [Term t]
	:	variable  { int index = varNames.indexOf($variable.text);
                if (index == -1){
                  index = varNames.size();
                  varNames.add($variable.text);
                }
                $t = tf.getVariable(index);
              }
  | constant {$t = tf.getConstant($constant.text);}
  ;
  
constant
  : STRING | INT | FLOAT;
	
comparison
	:	'=' | '!=' | '<' | '>' | '>=' | '<=' | 'LIKE' | null_comparison;
	
null_comparison returns [String text]
	:	'IS' 'NULL' {$text = "IS";}
	| 	'IS' 'NOT' 'NULL' {$text = "IS NOT";}
  ;
	
variable
	: ID;
  
predicateName
	: ID;
	
  
ID	:	('a'..'z' | 'A'..'Z' | '_' | '$') ( ('a'..'z' | 'A'..'Z') | ('0'..'9') | '-' | '_' | '#' )* 
	|	'`' (~('\''|'\n'|'\r'|'`')) ( (~('\''|'\n'|'\r'|'`')) )* '`' 
	|	'?'('0'..'9')+ ;
FLOAT	:	('0'..'9')+ '.' ('0'..'9')+ ;
INT	:	('0'..'9')+ ;
NUMERIC	:	(INT | FLOAT) 'E' ('+' | '-')? INT;
STRING	:	'"' (~('"'|'\n'|'\r'))*  '"'
	|	'\'' (~('\''|'\n'|'\r'))*  '\'';
WS	:	(' ' | '\t' | '\r' | '\n' ) {skip();} ;
COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {skip();}
    |   '/*' ( options {greedy=false;} : . )* '*/' {skip();}
    ;