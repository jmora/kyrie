// $ANTLR 3.2 Sep 23, 2009 12:02:23 /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g 2009-12-04 13:21:58

package es.upm.fi.dia.oeg.kyrie.parser;

import java.util.LinkedList;
import java.util.List;
import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;


public class ConjunctiveQueriesLexer extends Lexer {
	public static final int SENTENCE = 4;
	public static final int T__12 = 12;
	public static final int INT = 8;
	public static final int NUMBER = 6;
	public static final int EOF = -1;
	public static final int WS = 10;
	public static final int T__13 = 13;
	public static final int ALPHA = 7;
	public static final int CHAR = 9;
	public static final int T__16 = 16;
	public static final int ALPHAVAR = 5;
	public static final int T__14 = 14;
	public static final int T__11 = 11;
	public static final int T__15 = 15;

	private List<String> errors = new LinkedList<String>();

	@Override
	public void displayRecognitionError(String[] tokenNames, RecognitionException e) {
		String hdr = this.getErrorHeader(e);
		String msg = this.getErrorMessage(e, tokenNames);
		this.errors.add(hdr + " " + msg);
	}

	public List<String> getErrors() {
		return this.errors;
	}

	// delegates
	// delegators

	public ConjunctiveQueriesLexer() {
		;
	}

	public ConjunctiveQueriesLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}

	public ConjunctiveQueriesLexer(CharStream input, RecognizerSharedState state) {
		super(input, state);

	}

	@Override
	public String getGrammarFileName() {
		return "/Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g";
	}

	// $ANTLR start "T__11"
	public final void mT__11() throws RecognitionException {
		try {
			int _type = ConjunctiveQueriesLexer.T__11;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:24:7:
			// ( '<-' )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:24:9:
			// '<-'
			{
				this.match("<-");

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__11"

	// $ANTLR start "T__12"
	public final void mT__12() throws RecognitionException {
		try {
			int _type = ConjunctiveQueriesLexer.T__12;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:25:7:
			// ( ',' )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:25:9:
			// ','
			{
				this.match(',');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__12"

	// $ANTLR start "T__13"
	public final void mT__13() throws RecognitionException {
		try {
			int _type = ConjunctiveQueriesLexer.T__13;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:26:7:
			// ( 'Q' )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:26:9:
			// 'Q'
			{
				this.match('Q');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__13"

	// $ANTLR start "T__14"
	public final void mT__14() throws RecognitionException {
		try {
			int _type = ConjunctiveQueriesLexer.T__14;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:27:7:
			// ( '(' )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:27:9:
			// '('
			{
				this.match('(');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__14"

	// $ANTLR start "T__15"
	public final void mT__15() throws RecognitionException {
		try {
			int _type = ConjunctiveQueriesLexer.T__15;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:28:7:
			// ( ')' )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:28:9:
			// ')'
			{
				this.match(')');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__15"

	// $ANTLR start "T__16"
	public final void mT__16() throws RecognitionException {
		try {
			int _type = ConjunctiveQueriesLexer.T__16;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:29:7:
			// ( '?' )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:29:9:
			// '?'
			{
				this.match('?');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "T__16"

	// $ANTLR start "NUMBER"
	public final void mNUMBER() throws RecognitionException {
		try {
			int _type = ConjunctiveQueriesLexer.NUMBER;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:326:9:
			// ( ( '0' .. '9' )+ )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:326:11:
			// ( '0' .. '9' )+
			{
				// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:326:11:
				// ( '0' .. '9' )+
				int cnt1 = 0;
				loop1: do {
					int alt1 = 2;
					int LA1_0 = this.input.LA(1);

					if (LA1_0 >= '0' && LA1_0 <= '9')
						alt1 = 1;

					switch (alt1) {
						case 1:
						// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:326:11:
						// '0' .. '9'
						{
							this.matchRange('0', '9');

						}
							break;

						default:
							if (cnt1 >= 1)
								break loop1;
							EarlyExitException eee = new EarlyExitException(1, this.input);
							throw eee;
					}
					cnt1++;
				} while (true);

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "NUMBER"

	// $ANTLR start "SENTENCE"
	public final void mSENTENCE() throws RecognitionException {
		try {
			int _type = ConjunctiveQueriesLexer.SENTENCE;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:328:10:
			// ( '\"' ALPHAVAR ( ' ' ALPHAVAR )* '\"' )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:328:12:
			// '\"' ALPHAVAR ( ' ' ALPHAVAR )* '\"'
			{
				this.match('\"');
				this.mALPHAVAR();
				// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:328:25:
				// ( ' ' ALPHAVAR )*
				loop2: do {
					int alt2 = 2;
					int LA2_0 = this.input.LA(1);

					if (LA2_0 == ' ')
						alt2 = 1;

					switch (alt2) {
						case 1:
						// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:328:26:
						// ' ' ALPHAVAR
						{
							this.match(' ');
							this.mALPHAVAR();

						}
							break;

						default:
							break loop2;
					}
				} while (true);

				this.match('\"');

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "SENTENCE"

	// $ANTLR start "ALPHAVAR"
	public final void mALPHAVAR() throws RecognitionException {
		try {
			int _type = ConjunctiveQueriesLexer.ALPHAVAR;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:330:11:
			// ( ( ALPHA | INT | CHAR )+ )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:330:13:
			// ( ALPHA | INT | CHAR )+
			{
				// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:330:13:
				// ( ALPHA | INT | CHAR )+
				int cnt3 = 0;
				loop3: do {
					int alt3 = 4;
					switch (this.input.LA(1)) {
						case 'A':
						case 'B':
						case 'C':
						case 'D':
						case 'E':
						case 'F':
						case 'G':
						case 'H':
						case 'I':
						case 'J':
						case 'K':
						case 'L':
						case 'M':
						case 'N':
						case 'O':
						case 'P':
						case 'Q':
						case 'R':
						case 'S':
						case 'T':
						case 'U':
						case 'V':
						case 'W':
						case 'X':
						case 'Y':
						case 'Z':
						case 'a':
						case 'b':
						case 'c':
						case 'd':
						case 'e':
						case 'f':
						case 'g':
						case 'h':
						case 'i':
						case 'j':
						case 'k':
						case 'l':
						case 'm':
						case 'n':
						case 'o':
						case 'p':
						case 'q':
						case 'r':
						case 's':
						case 't':
						case 'u':
						case 'v':
						case 'w':
						case 'x':
						case 'y':
						case 'z': {
							alt3 = 1;
						}
							break;
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9': {
							alt3 = 2;
						}
							break;
						case '!':
						case '#':
						case '%':
						case '&':
						case '*':
						case '+':
						case '-':
						case '.':
						case ':':
						case '=':
						case '@':
						case '[':
						case ']':
						case '_': {
							alt3 = 3;
						}
							break;

					}

					switch (alt3) {
						case 1:
						// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:330:14:
						// ALPHA
						{
							this.mALPHA();

						}
							break;
						case 2:
						// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:330:22:
						// INT
						{
							this.mINT();

						}
							break;
						case 3:
						// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:330:28:
						// CHAR
						{
							this.mCHAR();

						}
							break;

						default:
							if (cnt3 >= 1)
								break loop3;
							EarlyExitException eee = new EarlyExitException(3, this.input);
							throw eee;
					}
					cnt3++;
				} while (true);

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "ALPHAVAR"

	// $ANTLR start "CHAR"
	public final void mCHAR() throws RecognitionException {
		try {
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:333:8:
			// ( ( '[' | ']' | '_' | '-' | '*' | '&' | '@' | '!' | '#' | '%' |
			// '+' | '=' | ':' | '.' ) )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:333:12:
			// ( '[' | ']' | '_' | '-' | '*' | '&' | '@' | '!' | '#' | '%' | '+'
			// | '=' | ':' | '.' )
			{
				if (this.input.LA(1) == '!' || this.input.LA(1) == '#' || this.input.LA(1) >= '%' && this.input.LA(1) <= '&' || this.input.LA(1) >= '*'
						&& this.input.LA(1) <= '+' || this.input.LA(1) >= '-' && this.input.LA(1) <= '.' || this.input.LA(1) == ':' || this.input.LA(1) == '='
						|| this.input.LA(1) == '@' || this.input.LA(1) == '[' || this.input.LA(1) == ']' || this.input.LA(1) == '_')
					this.input.consume();
				else {
					MismatchedSetException mse = new MismatchedSetException(null, this.input);
					this.recover(mse);
					throw mse;
				}

			}

		} finally {}
	}

	// $ANTLR end "CHAR"

	// $ANTLR start "ALPHA"
	public final void mALPHA() throws RecognitionException {
		try {
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:336:9:
			// ( ( 'a' .. 'z' | 'A' .. 'Z' )+ )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:336:13:
			// ( 'a' .. 'z' | 'A' .. 'Z' )+
			{
				// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:336:13:
				// ( 'a' .. 'z' | 'A' .. 'Z' )+
				int cnt4 = 0;
				loop4: do {
					int alt4 = 2;
					int LA4_0 = this.input.LA(1);

					if (LA4_0 >= 'A' && LA4_0 <= 'Z' || LA4_0 >= 'a' && LA4_0 <= 'z')
						alt4 = 1;

					switch (alt4) {
						case 1:
						// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:
						{
							if (this.input.LA(1) >= 'A' && this.input.LA(1) <= 'Z' || this.input.LA(1) >= 'a' && this.input.LA(1) <= 'z')
								this.input.consume();
							else {
								MismatchedSetException mse = new MismatchedSetException(null, this.input);
								this.recover(mse);
								throw mse;
							}

						}
							break;

						default:
							if (cnt4 >= 1)
								break loop4;
							EarlyExitException eee = new EarlyExitException(4, this.input);
							throw eee;
					}
					cnt4++;
				} while (true);

			}

		} finally {}
	}

	// $ANTLR end "ALPHA"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:339:7:
			// ( ( '0' .. '9' )+ )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:339:11:
			// ( '0' .. '9' )+
			{
				// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:339:11:
				// ( '0' .. '9' )+
				int cnt5 = 0;
				loop5: do {
					int alt5 = 2;
					int LA5_0 = this.input.LA(1);

					if (LA5_0 >= '0' && LA5_0 <= '9')
						alt5 = 1;

					switch (alt5) {
						case 1:
						// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:339:11:
						// '0' .. '9'
						{
							this.matchRange('0', '9');

						}
							break;

						default:
							if (cnt5 >= 1)
								break loop5;
							EarlyExitException eee = new EarlyExitException(5, this.input);
							throw eee;
					}
					cnt5++;
				} while (true);

			}

		} finally {}
	}

	// $ANTLR end "INT"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = ConjunctiveQueriesLexer.WS;
			int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:341:7:
			// ( ( ' ' | '\\t' | ( '\\r' | '\\r\\n' ) )+ )
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:341:11:
			// ( ' ' | '\\t' | ( '\\r' | '\\r\\n' ) )+
			{
				// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:341:11:
				// ( ' ' | '\\t' | ( '\\r' | '\\r\\n' ) )+
				int cnt7 = 0;
				loop7: do {
					int alt7 = 4;
					switch (this.input.LA(1)) {
						case ' ': {
							alt7 = 1;
						}
							break;
						case '\t': {
							alt7 = 2;
						}
							break;
						case '\r': {
							alt7 = 3;
						}
							break;

					}

					switch (alt7) {
						case 1:
						// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:341:12:
						// ' '
						{
							this.match(' ');

						}
							break;
						case 2:
						// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:341:16:
						// '\\t'
						{
							this.match('\t');

						}
							break;
						case 3:
						// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:341:21:
						// ( '\\r' | '\\r\\n' )
						{
							// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:341:21:
							// ( '\\r' | '\\r\\n' )
							int alt6 = 2;
							int LA6_0 = this.input.LA(1);

							if (LA6_0 == '\r') {
								int LA6_1 = this.input.LA(2);

								if (LA6_1 == '\n')
									alt6 = 2;
								else
									alt6 = 1;
							} else {
								NoViableAltException nvae = new NoViableAltException("", 6, 0, this.input);

								throw nvae;
							}
							switch (alt6) {
								case 1:
								// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:341:22:
								// '\\r'
								{
									this.match('\r');

								}
									break;
								case 2:
								// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:341:27:
								// '\\r\\n'
								{
									this.match("\r\n");

								}
									break;

							}

						}
							break;

						default:
							if (cnt7 >= 1)
								break loop7;
							EarlyExitException eee = new EarlyExitException(7, this.input);
							throw eee;
					}
					cnt7++;
				} while (true);

				this.skip();

			}

			this.state.type = _type;
			this.state.channel = _channel;
		} finally {}
	}

	// $ANTLR end "WS"

	@Override
	public void mTokens() throws RecognitionException {
		// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:1:8:
		// ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | NUMBER | SENTENCE |
		// ALPHAVAR | WS )
		int alt8 = 10;
		alt8 = this.dfa8.predict(this.input);
		switch (alt8) {
			case 1:
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:1:10:
			// T__11
			{
				this.mT__11();

			}
				break;
			case 2:
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:1:16:
			// T__12
			{
				this.mT__12();

			}
				break;
			case 3:
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:1:22:
			// T__13
			{
				this.mT__13();

			}
				break;
			case 4:
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:1:28:
			// T__14
			{
				this.mT__14();

			}
				break;
			case 5:
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:1:34:
			// T__15
			{
				this.mT__15();

			}
				break;
			case 6:
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:1:40:
			// T__16
			{
				this.mT__16();

			}
				break;
			case 7:
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:1:46:
			// NUMBER
			{
				this.mNUMBER();

			}
				break;
			case 8:
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:1:53:
			// SENTENCE
			{
				this.mSENTENCE();

			}
				break;
			case 9:
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:1:62:
			// ALPHAVAR
			{
				this.mALPHAVAR();

			}
				break;
			case 10:
			// /Users/hekanibru/Documents/DPhil/Prototype/Grammar/ConjunctiveQueries.g:1:71:
			// WS
			{
				this.mWS();

			}
				break;

		}

	}

	protected DFA8 dfa8 = new DFA8(this);
	static final String DFA8_eotS = "\3\uffff\1\13\3\uffff\1\14\5\uffff";
	static final String DFA8_eofS = "\15\uffff";
	static final String DFA8_minS = "\1\11\2\uffff\1\41\3\uffff\1\41\5\uffff";
	static final String DFA8_maxS = "\1\172\2\uffff\1\172\3\uffff\1\172\5\uffff";
	static final String DFA8_acceptS = "\1\uffff\1\1\1\2\1\uffff\1\4\1\5\1\6\1\uffff\1\10\1\11\1\12\1\3" + "\1\7";
	static final String DFA8_specialS = "\15\uffff}>";
	static final String[] DFA8_transitionS = {
			"\1\12\3\uffff\1\12\22\uffff\1\12\1\11\1\10\1\11\1\uffff\2\11" + "\1\uffff\1\4\1\5\2\11\1\2\2\11\1\uffff\12\7\1\11\1\uffff\1\1"
					+ "\1\11\1\uffff\1\6\21\11\1\3\12\11\1\uffff\1\11\1\uffff\1\11" + "\1\uffff\32\11",
			"",
			"",
			"\1\11\1\uffff\1\11\1\uffff\2\11\3\uffff\2\11\1\uffff\2\11\1" + "\uffff\13\11\2\uffff\1\11\2\uffff\34\11\1\uffff\1\11\1\uffff"
					+ "\1\11\1\uffff\32\11",
			"",
			"",
			"",
			"\1\11\1\uffff\1\11\1\uffff\2\11\3\uffff\2\11\1\uffff\2\11\1" + "\uffff\12\7\1\11\2\uffff\1\11\2\uffff\34\11\1\uffff\1\11\1\uffff"
					+ "\1\11\1\uffff\32\11", "", "", "", "", "" };

	static final short[] DFA8_eot = DFA.unpackEncodedString(ConjunctiveQueriesLexer.DFA8_eotS);
	static final short[] DFA8_eof = DFA.unpackEncodedString(ConjunctiveQueriesLexer.DFA8_eofS);
	static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(ConjunctiveQueriesLexer.DFA8_minS);
	static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(ConjunctiveQueriesLexer.DFA8_maxS);
	static final short[] DFA8_accept = DFA.unpackEncodedString(ConjunctiveQueriesLexer.DFA8_acceptS);
	static final short[] DFA8_special = DFA.unpackEncodedString(ConjunctiveQueriesLexer.DFA8_specialS);
	static final short[][] DFA8_transition;

	static {
		int numStates = ConjunctiveQueriesLexer.DFA8_transitionS.length;
		DFA8_transition = new short[numStates][];
		for (int i = 0; i < numStates; i++)
			ConjunctiveQueriesLexer.DFA8_transition[i] = DFA.unpackEncodedString(ConjunctiveQueriesLexer.DFA8_transitionS[i]);
	}

	class DFA8 extends DFA {

		public DFA8(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 8;
			this.eot = ConjunctiveQueriesLexer.DFA8_eot;
			this.eof = ConjunctiveQueriesLexer.DFA8_eof;
			this.min = ConjunctiveQueriesLexer.DFA8_min;
			this.max = ConjunctiveQueriesLexer.DFA8_max;
			this.accept = ConjunctiveQueriesLexer.DFA8_accept;
			this.special = ConjunctiveQueriesLexer.DFA8_special;
			this.transition = ConjunctiveQueriesLexer.DFA8_transition;
		}

		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | NUMBER | SENTENCE | ALPHAVAR | WS );";
		}
	}

}
