package es.upm.fi.dia.oeg.kyrie.rewriter;

public abstract class SelectionFunction {
	public abstract void selectAtoms(Clause c);

	public abstract boolean isToBePruned(Clause c);
}
