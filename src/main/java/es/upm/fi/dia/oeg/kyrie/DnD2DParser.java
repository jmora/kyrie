// $ANTLR 3.1 DnD2D.g 2012-06-07 14:41:57

package es.upm.fi.dia.oeg.kyrie;

import java.util.ArrayList;

import org.antlr.runtime.BitSet;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.Parser;
import org.antlr.runtime.ParserRuleReturnScope;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.TokenStream;

import es.upm.fi.dia.oeg.kyrie.rewriter.Clause;
import es.upm.fi.dia.oeg.kyrie.rewriter.FunctionalTerm;
import es.upm.fi.dia.oeg.kyrie.rewriter.Term;
import es.upm.fi.dia.oeg.kyrie.rewriter.TermFactory;


public class DnD2DParser extends Parser {
	public static final String[] tokenNames = new String[] { "<invalid>", "<EOR>", "<DOWN>", "<UP>", "STRING", "INT", "FLOAT", "ID", "NUMERIC", "WS",
			"COMMENT", "'<-'", "':-'", "'^'", "','", "'('", "')'", "'='", "'!='", "'<'", "'>'", "'>='", "'<='", "'LIKE'", "'IS'", "'NULL'", "'NOT'" };
	public static final int T__26 = 26;
	public static final int T__25 = 25;
	public static final int T__24 = 24;
	public static final int T__23 = 23;
	public static final int T__22 = 22;
	public static final int T__21 = 21;
	public static final int T__20 = 20;
	public static final int FLOAT = 6;
	public static final int INT = 5;
	public static final int NUMERIC = 8;
	public static final int ID = 7;
	public static final int EOF = -1;
	public static final int T__19 = 19;
	public static final int WS = 9;
	public static final int T__16 = 16;
	public static final int T__15 = 15;
	public static final int T__18 = 18;
	public static final int T__17 = 17;
	public static final int T__12 = 12;
	public static final int T__11 = 11;
	public static final int T__14 = 14;
	public static final int T__13 = 13;
	public static final int COMMENT = 10;
	public static final int STRING = 4;

	// delegates
	// delegators

	public DnD2DParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}

	public DnD2DParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);

	}

	@Override
	public String[] getTokenNames() {
		return DnD2DParser.tokenNames;
	}

	@Override
	public String getGrammarFileName() {
		return "DnD2D.g";
	}

	public ArrayList<String> varNames = new ArrayList<String>();
	TermFactory tf = new TermFactory();
	boolean queryFound = false;

	// $ANTLR start "program"
	// DnD2D.g:23:1: program returns [ArrayList<Clause> program] : ( rule )*
	// EOF ;
	public final ArrayList<Clause> program() throws RecognitionException {
		ArrayList<Clause> program = null;

		Clause rule1 = null;

		program = new ArrayList<Clause>();
		try {
			// DnD2D.g:25:3: ( ( rule )* EOF )
			// DnD2D.g:25:5: ( rule )* EOF
			{
				// DnD2D.g:25:5: ( rule )*
				loop1: do {
					int alt1 = 2;
					int LA1_0 = this.input.LA(1);

					if (LA1_0 == DnD2DParser.ID)
						alt1 = 1;

					switch (alt1) {
						case 1:
						// DnD2D.g:25:6: rule
						{
							this.pushFollow(DnD2DParser.FOLLOW_rule_in_program44);
							rule1 = this.rule();

							this.state._fsp--;

							program.add(rule1);

						}
							break;

						default:
							break loop1;
					}
				} while (true);

				this.match(this.input, DnD2DParser.EOF, DnD2DParser.FOLLOW_EOF_in_program50);

			}

		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return program;
	}

	// $ANTLR end "program"

	// $ANTLR start "rule"
	// DnD2D.g:27:1: rule returns [Clause clause] : relation_predicate ( '<-'
	// | ':-' ) rule_body ;
	public final Clause rule() throws RecognitionException {
		Clause clause = null;

		ArrayList<Term> rule_body2 = null;

		Term relation_predicate3 = null;

		try {
			// DnD2D.g:28:2: ( relation_predicate ( '<-' | ':-' ) rule_body )
			// DnD2D.g:28:4: relation_predicate ( '<-' | ':-' ) rule_body
			{
				this.pushFollow(DnD2DParser.FOLLOW_relation_predicate_in_rule64);
				relation_predicate3 = this.relation_predicate();

				this.state._fsp--;

				if (this.input.LA(1) >= 11 && this.input.LA(1) <= 12) {
					this.input.consume();
					this.state.errorRecovery = false;
				} else {
					MismatchedSetException mse = new MismatchedSetException(null, this.input);
					throw mse;
				}

				this.pushFollow(DnD2DParser.FOLLOW_rule_body_in_rule72);
				rule_body2 = this.rule_body();

				this.state._fsp--;

				clause = new Clause(rule_body2.toArray(new Term[0]), relation_predicate3);

			}

		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return clause;
	}

	// $ANTLR end "rule"

	// $ANTLR start "rule_body"
	// DnD2D.g:30:1: rule_body returns [ArrayList<Term> body] : p1= predicate
	// ( ( '^' | ',' ) p2= predicate )* ;
	public final ArrayList<Term> rule_body() throws RecognitionException {
		ArrayList<Term> body = null;

		Term p1 = null;

		Term p2 = null;

		body = new ArrayList<Term>();
		try {
			// DnD2D.g:32:2: (p1= predicate ( ( '^' | ',' ) p2= predicate )* )
			// DnD2D.g:32:4: p1= predicate ( ( '^' | ',' ) p2= predicate )*
			{
				this.pushFollow(DnD2DParser.FOLLOW_predicate_in_rule_body96);
				p1 = this.predicate();

				this.state._fsp--;

				body.add(p1);
				// DnD2D.g:33:5: ( ( '^' | ',' ) p2= predicate )*
				loop2: do {
					int alt2 = 2;
					int LA2_0 = this.input.LA(1);

					if (LA2_0 >= 13 && LA2_0 <= 14)
						alt2 = 1;

					switch (alt2) {
						case 1:
						// DnD2D.g:33:6: ( '^' | ',' ) p2= predicate
						{
							if (this.input.LA(1) >= 13 && this.input.LA(1) <= 14) {
								this.input.consume();
								this.state.errorRecovery = false;
							} else {
								MismatchedSetException mse = new MismatchedSetException(null, this.input);
								throw mse;
							}

							this.pushFollow(DnD2DParser.FOLLOW_predicate_in_rule_body113);
							p2 = this.predicate();

							this.state._fsp--;

							body.add(p2);

						}
							break;

						default:
							break loop2;
					}
				} while (true);

			}

		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return body;
	}

	// $ANTLR end "rule_body"

	// $ANTLR start "predicate"
	// DnD2D.g:36:1: predicate returns [Term t] : ( relation_predicate |
	// builtin_predicate );
	public final Term predicate() throws RecognitionException {
		Term t = null;

		Term relation_predicate4 = null;

		Term builtin_predicate5 = null;

		try {
			// DnD2D.g:37:2: ( relation_predicate | builtin_predicate )
			int alt3 = 2;
			int LA3_0 = this.input.LA(1);

			if (LA3_0 == DnD2DParser.ID)
				alt3 = 1;
			else if (LA3_0 == 15)
				alt3 = 2;
			else {
				NoViableAltException nvae = new NoViableAltException("", 3, 0, this.input);

				throw nvae;
			}
			switch (alt3) {
				case 1:
				// DnD2D.g:37:4: relation_predicate
				{
					this.pushFollow(DnD2DParser.FOLLOW_relation_predicate_in_predicate134);
					relation_predicate4 = this.relation_predicate();

					this.state._fsp--;

					t = relation_predicate4;

				}
					break;
				case 2:
				// DnD2D.g:38:5: builtin_predicate
				{
					this.pushFollow(DnD2DParser.FOLLOW_builtin_predicate_in_predicate143);
					builtin_predicate5 = this.builtin_predicate();

					this.state._fsp--;

					t = builtin_predicate5;

				}
					break;

			}
		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return t;
	}

	// $ANTLR end "predicate"

	// $ANTLR start "relation_predicate"
	// DnD2D.g:41:1: relation_predicate returns [Term t] : predicateName '('
	// c1= column_value ( ',' c2= column_value )* ')' ;
	public final Term relation_predicate() throws RecognitionException {
		Term t = null;

		Term c1 = null;

		Term c2 = null;

		DnD2DParser.predicateName_return predicateName6 = null;

		ArrayList<Term> body = new ArrayList<Term>();
		try {
			// DnD2D.g:43:2: ( predicateName '(' c1= column_value ( ',' c2=
			// column_value )* ')' )
			// DnD2D.g:43:4: predicateName '(' c1= column_value ( ',' c2=
			// column_value )* ')'
			{
				this.pushFollow(DnD2DParser.FOLLOW_predicateName_in_relation_predicate169);
				predicateName6 = this.predicateName();

				this.state._fsp--;

				this.match(this.input, 15, DnD2DParser.FOLLOW_15_in_relation_predicate171);
				this.pushFollow(DnD2DParser.FOLLOW_column_value_in_relation_predicate175);
				c1 = this.column_value();

				this.state._fsp--;

				body.add(c1);
				// DnD2D.g:43:57: ( ',' c2= column_value )*
				loop4: do {
					int alt4 = 2;
					int LA4_0 = this.input.LA(1);

					if (LA4_0 == 14)
						alt4 = 1;

					switch (alt4) {
						case 1:
						// DnD2D.g:43:58: ',' c2= column_value
						{
							this.match(this.input, 14, DnD2DParser.FOLLOW_14_in_relation_predicate180);
							this.pushFollow(DnD2DParser.FOLLOW_column_value_in_relation_predicate184);
							c2 = this.column_value();

							this.state._fsp--;

							body.add(c2);

						}
							break;

						default:
							break loop4;
					}
				} while (true);

				this.match(this.input, 16, DnD2DParser.FOLLOW_16_in_relation_predicate190);
				t = new FunctionalTerm(predicateName6 != null ? this.input.toString(predicateName6.start, predicateName6.stop) : null,
						body.toArray(new Term[0]));

			}

		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return t;
	}

	// $ANTLR end "relation_predicate"

	// $ANTLR start "builtin_predicate"
	// DnD2D.g:47:1: builtin_predicate returns [Term t] : '(' v1= column_value
	// comparison (v2= column_value )? ')' ;
	public final Term builtin_predicate() throws RecognitionException {
		Term t = null;

		Term v1 = null;

		Term v2 = null;

		DnD2DParser.comparison_return comparison7 = null;

		try {
			// DnD2D.g:48:2: ( '(' v1= column_value comparison (v2=
			// column_value )? ')' )
			// DnD2D.g:48:4: '(' v1= column_value comparison (v2= column_value
			// )? ')'
			{
				this.match(this.input, 15, DnD2DParser.FOLLOW_15_in_builtin_predicate212);
				this.pushFollow(DnD2DParser.FOLLOW_column_value_in_builtin_predicate216);
				v1 = this.column_value();

				this.state._fsp--;

				this.pushFollow(DnD2DParser.FOLLOW_comparison_in_builtin_predicate218);
				comparison7 = this.comparison();

				this.state._fsp--;

				// DnD2D.g:48:37: (v2= column_value )?
				int alt5 = 2;
				int LA5_0 = this.input.LA(1);

				if (LA5_0 >= DnD2DParser.STRING && LA5_0 <= DnD2DParser.ID)
					alt5 = 1;
				switch (alt5) {
					case 1:
					// DnD2D.g:48:37: v2= column_value
					{
						this.pushFollow(DnD2DParser.FOLLOW_column_value_in_builtin_predicate222);
						v2 = this.column_value();

						this.state._fsp--;

					}
						break;

				}

				this.match(this.input, 16, DnD2DParser.FOLLOW_16_in_builtin_predicate225);

				if (v2 == null)
					t = this.tf.getFunctionalTerm(comparison7 != null ? this.input.toString(comparison7.start, comparison7.stop) : null, v1);
				else
					t = this.tf.getFunctionalTerm(comparison7 != null ? this.input.toString(comparison7.start, comparison7.stop) : null, v1, v2);

			}

		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return t;
	}

	// $ANTLR end "builtin_predicate"

	// $ANTLR start "column_value"
	// DnD2D.g:55:1: column_value returns [Term t] : ( variable | constant );
	public final Term column_value() throws RecognitionException {
		Term t = null;

		DnD2DParser.variable_return variable8 = null;

		DnD2DParser.constant_return constant9 = null;

		try {
			// DnD2D.g:56:2: ( variable | constant )
			int alt6 = 2;
			int LA6_0 = this.input.LA(1);

			if (LA6_0 == DnD2DParser.ID)
				alt6 = 1;
			else if (LA6_0 >= DnD2DParser.STRING && LA6_0 <= DnD2DParser.FLOAT)
				alt6 = 2;
			else {
				NoViableAltException nvae = new NoViableAltException("", 6, 0, this.input);

				throw nvae;
			}
			switch (alt6) {
				case 1:
				// DnD2D.g:56:4: variable
				{
					this.pushFollow(DnD2DParser.FOLLOW_variable_in_column_value241);
					variable8 = this.variable();

					this.state._fsp--;

					int index = this.varNames.indexOf(variable8 != null ? this.input.toString(variable8.start, variable8.stop) : null);
					if (index == -1) {
						index = this.varNames.size();
						this.varNames.add(variable8 != null ? this.input.toString(variable8.start, variable8.stop) : null);
					}
					t = this.tf.getVariable(index);

				}
					break;
				case 2:
				// DnD2D.g:63:5: constant
				{
					this.pushFollow(DnD2DParser.FOLLOW_constant_in_column_value250);
					constant9 = this.constant();

					this.state._fsp--;

					t = this.tf.getConstant(constant9 != null ? this.input.toString(constant9.start, constant9.stop) : null);

				}
					break;

			}
		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return t;
	}

	// $ANTLR end "column_value"

	public static class constant_return extends ParserRuleReturnScope {};

	// $ANTLR start "constant"
	// DnD2D.g:66:1: constant : ( STRING | INT | FLOAT );
	public final DnD2DParser.constant_return constant() throws RecognitionException {
		DnD2DParser.constant_return retval = new DnD2DParser.constant_return();
		retval.start = this.input.LT(1);

		try {
			// DnD2D.g:67:3: ( STRING | INT | FLOAT )
			// DnD2D.g:
			{
				if (this.input.LA(1) >= DnD2DParser.STRING && this.input.LA(1) <= DnD2DParser.FLOAT) {
					this.input.consume();
					this.state.errorRecovery = false;
				} else {
					MismatchedSetException mse = new MismatchedSetException(null, this.input);
					throw mse;
				}

			}

			retval.stop = this.input.LT(-1);

		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return retval;
	}

	// $ANTLR end "constant"

	public static class comparison_return extends ParserRuleReturnScope {};

	// $ANTLR start "comparison"
	// DnD2D.g:69:1: comparison : ( '=' | '!=' | '<' | '>' | '>=' | '<=' |
	// 'LIKE' | null_comparison );
	public final DnD2DParser.comparison_return comparison() throws RecognitionException {
		DnD2DParser.comparison_return retval = new DnD2DParser.comparison_return();
		retval.start = this.input.LT(1);

		try {
			// DnD2D.g:70:2: ( '=' | '!=' | '<' | '>' | '>=' | '<=' | 'LIKE' |
			// null_comparison )
			int alt7 = 8;
			switch (this.input.LA(1)) {
				case 17: {
					alt7 = 1;
				}
					break;
				case 18: {
					alt7 = 2;
				}
					break;
				case 19: {
					alt7 = 3;
				}
					break;
				case 20: {
					alt7 = 4;
				}
					break;
				case 21: {
					alt7 = 5;
				}
					break;
				case 22: {
					alt7 = 6;
				}
					break;
				case 23: {
					alt7 = 7;
				}
					break;
				case 24: {
					alt7 = 8;
				}
					break;
				default:
					NoViableAltException nvae = new NoViableAltException("", 7, 0, this.input);

					throw nvae;
			}

			switch (alt7) {
				case 1:
				// DnD2D.g:70:4: '='
				{
					this.match(this.input, 17, DnD2DParser.FOLLOW_17_in_comparison285);

				}
					break;
				case 2:
				// DnD2D.g:70:10: '!='
				{
					this.match(this.input, 18, DnD2DParser.FOLLOW_18_in_comparison289);

				}
					break;
				case 3:
				// DnD2D.g:70:17: '<'
				{
					this.match(this.input, 19, DnD2DParser.FOLLOW_19_in_comparison293);

				}
					break;
				case 4:
				// DnD2D.g:70:23: '>'
				{
					this.match(this.input, 20, DnD2DParser.FOLLOW_20_in_comparison297);

				}
					break;
				case 5:
				// DnD2D.g:70:29: '>='
				{
					this.match(this.input, 21, DnD2DParser.FOLLOW_21_in_comparison301);

				}
					break;
				case 6:
				// DnD2D.g:70:36: '<='
				{
					this.match(this.input, 22, DnD2DParser.FOLLOW_22_in_comparison305);

				}
					break;
				case 7:
				// DnD2D.g:70:43: 'LIKE'
				{
					this.match(this.input, 23, DnD2DParser.FOLLOW_23_in_comparison309);

				}
					break;
				case 8:
				// DnD2D.g:70:52: null_comparison
				{
					this.pushFollow(DnD2DParser.FOLLOW_null_comparison_in_comparison313);
					this.null_comparison();

					this.state._fsp--;

				}
					break;

			}
			retval.stop = this.input.LT(-1);

		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return retval;
	}

	// $ANTLR end "comparison"

	// $ANTLR start "null_comparison"
	// DnD2D.g:72:1: null_comparison returns [String text] : ( 'IS' 'NULL' |
	// 'IS' 'NOT' 'NULL' );
	public final String null_comparison() throws RecognitionException {
		String text = null;

		try {
			// DnD2D.g:73:2: ( 'IS' 'NULL' | 'IS' 'NOT' 'NULL' )
			int alt8 = 2;
			int LA8_0 = this.input.LA(1);

			if (LA8_0 == 24) {
				int LA8_1 = this.input.LA(2);

				if (LA8_1 == 25)
					alt8 = 1;
				else if (LA8_1 == 26)
					alt8 = 2;
				else {
					NoViableAltException nvae = new NoViableAltException("", 8, 1, this.input);

					throw nvae;
				}
			} else {
				NoViableAltException nvae = new NoViableAltException("", 8, 0, this.input);

				throw nvae;
			}
			switch (alt8) {
				case 1:
				// DnD2D.g:73:4: 'IS' 'NULL'
				{
					this.match(this.input, 24, DnD2DParser.FOLLOW_24_in_null_comparison327);
					this.match(this.input, 25, DnD2DParser.FOLLOW_25_in_null_comparison329);
					text = "IS";

				}
					break;
				case 2:
				// DnD2D.g:74:5: 'IS' 'NOT' 'NULL'
				{
					this.match(this.input, 24, DnD2DParser.FOLLOW_24_in_null_comparison337);
					this.match(this.input, 26, DnD2DParser.FOLLOW_26_in_null_comparison339);
					this.match(this.input, 25, DnD2DParser.FOLLOW_25_in_null_comparison341);
					text = "IS NOT";

				}
					break;

			}
		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return text;
	}

	// $ANTLR end "null_comparison"

	public static class variable_return extends ParserRuleReturnScope {};

	// $ANTLR start "variable"
	// DnD2D.g:77:1: variable : ID ;
	public final DnD2DParser.variable_return variable() throws RecognitionException {
		DnD2DParser.variable_return retval = new DnD2DParser.variable_return();
		retval.start = this.input.LT(1);

		try {
			// DnD2D.g:78:2: ( ID )
			// DnD2D.g:78:4: ID
			{
				this.match(this.input, DnD2DParser.ID, DnD2DParser.FOLLOW_ID_in_variable356);

			}

			retval.stop = this.input.LT(-1);

		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return retval;
	}

	// $ANTLR end "variable"

	public static class predicateName_return extends ParserRuleReturnScope {};

	// $ANTLR start "predicateName"
	// DnD2D.g:80:1: predicateName : ID ;
	public final DnD2DParser.predicateName_return predicateName() throws RecognitionException {
		DnD2DParser.predicateName_return retval = new DnD2DParser.predicateName_return();
		retval.start = this.input.LT(1);

		try {
			// DnD2D.g:81:2: ( ID )
			// DnD2D.g:81:4: ID
			{
				this.match(this.input, DnD2DParser.ID, DnD2DParser.FOLLOW_ID_in_predicateName367);

			}

			retval.stop = this.input.LT(-1);

		} catch (RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {}
		return retval;
	}

	// $ANTLR end "predicateName"

	// Delegated rules

	public static final BitSet FOLLOW_rule_in_program44 = new BitSet(new long[] { 0x0000000000000080L });
	public static final BitSet FOLLOW_EOF_in_program50 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_relation_predicate_in_rule64 = new BitSet(new long[] { 0x0000000000001800L });
	public static final BitSet FOLLOW_set_in_rule66 = new BitSet(new long[] { 0x0000000000008080L });
	public static final BitSet FOLLOW_rule_body_in_rule72 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_predicate_in_rule_body96 = new BitSet(new long[] { 0x0000000000006002L });
	public static final BitSet FOLLOW_set_in_rule_body105 = new BitSet(new long[] { 0x0000000000008080L });
	public static final BitSet FOLLOW_predicate_in_rule_body113 = new BitSet(new long[] { 0x0000000000006002L });
	public static final BitSet FOLLOW_relation_predicate_in_predicate134 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_builtin_predicate_in_predicate143 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_predicateName_in_relation_predicate169 = new BitSet(new long[] { 0x0000000000008000L });
	public static final BitSet FOLLOW_15_in_relation_predicate171 = new BitSet(new long[] { 0x00000000000000F0L });
	public static final BitSet FOLLOW_column_value_in_relation_predicate175 = new BitSet(new long[] { 0x0000000000014000L });
	public static final BitSet FOLLOW_14_in_relation_predicate180 = new BitSet(new long[] { 0x00000000000000F0L });
	public static final BitSet FOLLOW_column_value_in_relation_predicate184 = new BitSet(new long[] { 0x0000000000014000L });
	public static final BitSet FOLLOW_16_in_relation_predicate190 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_15_in_builtin_predicate212 = new BitSet(new long[] { 0x00000000000000F0L });
	public static final BitSet FOLLOW_column_value_in_builtin_predicate216 = new BitSet(new long[] { 0x0000000001FE0000L });
	public static final BitSet FOLLOW_comparison_in_builtin_predicate218 = new BitSet(new long[] { 0x00000000000100F0L });
	public static final BitSet FOLLOW_column_value_in_builtin_predicate222 = new BitSet(new long[] { 0x0000000000010000L });
	public static final BitSet FOLLOW_16_in_builtin_predicate225 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_variable_in_column_value241 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_constant_in_column_value250 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_set_in_constant0 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_17_in_comparison285 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_18_in_comparison289 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_19_in_comparison293 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_20_in_comparison297 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_21_in_comparison301 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_22_in_comparison305 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_23_in_comparison309 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_null_comparison_in_comparison313 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_24_in_null_comparison327 = new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet FOLLOW_25_in_null_comparison329 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_24_in_null_comparison337 = new BitSet(new long[] { 0x0000000004000000L });
	public static final BitSet FOLLOW_26_in_null_comparison339 = new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet FOLLOW_25_in_null_comparison341 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_ID_in_variable356 = new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet FOLLOW_ID_in_predicateName367 = new BitSet(new long[] { 0x0000000000000002L });

}
