package es.upm.fi.dia.oeg.kyrie;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import com.hp.hpl.jena.query.Query;

import es.upm.fi.dia.oeg.kyrie.parser.ELHIOParser;
import es.upm.fi.dia.oeg.kyrie.rewriter.Clause;
import es.upm.fi.dia.oeg.kyrie.rewriter.Rewriter;
import es.upm.fi.dia.oeg.kyrie.rewriter.TermFactory;


public class KyrieLib {

	private Rewriter pw;
	private DatalogSPARQLConversor dsc;
	private static final TermFactory m_termFactory = new TermFactory();
	private ELHIOParser m_parser;

	private void init(Collection<Clause> clauses, Collection<String> mappings, boolean fullURIs) {
		this.m_parser = new ELHIOParser(KyrieLib.m_termFactory, fullURIs);
		if (mappings == null || mappings.isEmpty())
			this.pw = new Rewriter(new ArrayList<Clause>(clauses));
		else
			this.pw = new Rewriter(new ArrayList<Clause>(clauses), mappings);
		this.dsc = new DatalogSPARQLConversor();
	}

	public KyrieLib(Collection<Clause> clauses) {
		this.init(clauses, null, false);
	}

	public KyrieLib(Collection<Clause> clauses, boolean fullURIs) {
		this.init(clauses, null, fullURIs);
	}

	public KyrieLib(String ontologyFile) {
		this.init(this.readFileIntoClauses(ontologyFile), null, false);
	}

	public KyrieLib(String ontologyFile, boolean fullURIs) {
		this.init(this.readFileIntoClauses(ontologyFile), null, fullURIs);
	}

	public KyrieLib(String ontologyFile, String mappingsFile, boolean fullURIs) {
		ArrayList<String> mappedPredicates = new ArrayList<String>();
		if (mappingsFile.endsWith("txt"))
			try {
				mappedPredicates = new ArrayList<String>(KyrieTerminal.getMappedPredicatesFromTXT(mappingsFile));
			} catch (IOException e) {
				e.printStackTrace();
			}
		else
			mappedPredicates = new ArrayList<String>(KyrieTerminal.getMappedPredicatesFromR2O(mappingsFile));
		this.init(this.readFileIntoClauses(ontologyFile), mappedPredicates, fullURIs);
	}

	public KyrieLib(ArrayList<Clause> clauses, Collection<String> mappings, boolean fullURIs) {
		this.init(clauses, mappings, fullURIs);
	}

	public String rewriteDatalog(String datalog) {
		DnD2DLexer lexer = this.getLexer(datalog);
		DnD2DParser parser = new DnD2DParser(new CommonTokenStream(lexer));
		ArrayList<Clause> query = new ArrayList<Clause>();
		try {
			query = parser.program();
		} catch (RecognitionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return KyrieLib.getontostring(this.pw.rewrite(query));
	}

	public Collection<Clause> rewriteDatalogQuery(Collection<Clause> query) {
		return this.rewrite(query);
	}

	public ArrayList<Query> rewriteSPARQLQuery(Query query) {
		ArrayList<Clause> original = new ArrayList<Clause>();
		try {
			original = this.dsc.sparqlToDatalog(query);
		} catch (ConversorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<Clause> rewriting = new ArrayList<Clause>(this.rewrite(original));
		return this.dsc.datalogToSPARQL(rewriting);
	}

	public static String getontostring(ArrayList<Clause> ontology) {
		String ontostring = "";
		for (Clause c : ontology)
			ontostring += c.toString() + "\n";
		return ontostring;
	}

	private Collection<Clause> rewrite(Collection<Clause> query) {
		return this.pw.rewrite(new ArrayList<Clause>(query));
	}

	private ArrayList<Clause> readFileIntoClauses(String ontologyFile) {
		ArrayList<Clause> ontology = new ArrayList<Clause>();
		try {
			ontology = this.m_parser.getClauses(ontologyFile);
		} catch (Exception e) {
			// TODO: handle properly
			e.printStackTrace();
		}
		return ontology;
	}

	private DnD2DLexer getLexer(String datalog) {
		DnD2DLexer lexer = null;
		if (new File(datalog).exists())
			try {
				lexer = new DnD2DLexer(new ANTLRFileStream(datalog));
			} catch (IOException e) {
				e.printStackTrace();
				lexer = new DnD2DLexer(new ANTLRStringStream(datalog));
			}
		else
			lexer = new DnD2DLexer(new ANTLRStringStream(datalog));
		return lexer;
	}

}
