package es.upm.fi.dia.oeg.kyrie;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import es.upm.fi.dia.oeg.obdi.wrapper.AbstractConceptMapping;
import es.upm.fi.dia.oeg.obdi.wrapper.AbstractParser;
import es.upm.fi.dia.oeg.obdi.wrapper.IAttributeMapping;
import es.upm.fi.dia.oeg.obdi.wrapper.IMappingDocument;
import es.upm.fi.dia.oeg.obdi.wrapper.IRelationMapping;
import es.upm.fi.dia.oeg.obdi.wrapper.r2o.R2OParser;


public class MappingsExtractor {

	public static Set<String> getMappedPredicatesFromTXT(String string) throws IOException {
		HashSet<String> res = new HashSet<String>();
		FileInputStream in = new FileInputStream(string);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		while ((strLine = br.readLine()) != null)
			res.add(strLine);
		in.close();
		return res;
	}

	public static Set<String> getMappedPredicatesFromR2O(String r2oFile) {
		Set<String> result = new HashSet<String>();
		try {

			AbstractParser parser = new R2OParser();
			IMappingDocument mappingDocument = parser.parse(r2oFile);
			Collection<AbstractConceptMapping> mappedConcepts = mappingDocument.getConceptMappings();

			for (AbstractConceptMapping conceptMapping : mappedConcepts) {
				result.add(conceptMapping.getName());
				Collection<IAttributeMapping> attributeMappings = mappingDocument.getAttributeMappings(conceptMapping.getName(), null);
				for (IAttributeMapping attributeMapping : attributeMappings)
					result.add(attributeMapping.getAttributeName());

				Collection<IRelationMapping> relationMappings = mappingDocument.getRelationMappings(conceptMapping.getName(), null);
				for (IRelationMapping relationMapping : relationMappings)
					result.add(relationMapping.getRelationName());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
