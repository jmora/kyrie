package es.upm.fi.dia.oeg.kyrie;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;
import org.apache.log4j.Logger;

import es.upm.fi.dia.oeg.kyrie.parser.ELHIOParser;
import es.upm.fi.dia.oeg.kyrie.rewriter.Clause;
import es.upm.fi.dia.oeg.kyrie.rewriter.Rewriter;
import es.upm.fi.dia.oeg.kyrie.rewriter.TermFactory;
import es.upm.fi.dia.oeg.obdi.wrapper.AbstractConceptMapping;
import es.upm.fi.dia.oeg.obdi.wrapper.AbstractParser;
import es.upm.fi.dia.oeg.obdi.wrapper.IAttributeMapping;
import es.upm.fi.dia.oeg.obdi.wrapper.IMappingDocument;
import es.upm.fi.dia.oeg.obdi.wrapper.IRelationMapping;
import es.upm.fi.dia.oeg.obdi.wrapper.r2o.R2OParser;

public class KyrieTerminal {

	private static final TermFactory m_termFactory = new TermFactory();
	private static final ELHIOParser m_parser = new ELHIOParser(KyrieTerminal.m_termFactory, false);
	static Logger logger = Logger.getLogger(KyrieTerminal.class.getName());

	private static void checkArgs(String[] args) throws Exception {
		String err = "Use: java Requiem query.txt ontology.owl outPath [MappingsFile]";
		if (args.length < 2)
			throw new Exception(err);
	}

	public static Set<String> getMappedPredicatesFromR2O(String r2oFile) {
		Set<String> result = new HashSet<String>();
		try {
			AbstractParser parser = new R2OParser();
			IMappingDocument mappingDocument = parser.parse(r2oFile);
			Collection<AbstractConceptMapping> mappedConcepts = mappingDocument.getConceptMappings();

			for (AbstractConceptMapping conceptMapping : mappedConcepts) {
				result.add(conceptMapping.getName());
				Collection<IAttributeMapping> attributeMappings = mappingDocument.getAttributeMappings(conceptMapping.getName(), null);
				for (IAttributeMapping attributeMapping : attributeMappings)
					result.add(attributeMapping.getAttributeName());

				Collection<IRelationMapping> relationMappings = mappingDocument.getRelationMappings(conceptMapping.getName(), null);
				for (IRelationMapping relationMapping : relationMappings)
					result.add(relationMapping.getRelationName());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static Set<String> getMappedPredicatesFromTXT(String string) throws IOException {
		HashSet<String> res = new HashSet<String>();
		FileInputStream in = new FileInputStream(string);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		while ((strLine = br.readLine()) != null)
			res.add(strLine);
		in.close();
		return res;
	}

	/**
	 * @param args
	 *            0 - query file 1 - ontology file [2 - mappingsFile]
	 */
	public static void main(String[] args) throws Exception {

		KyrieTerminal.checkArgs(args);
		Set<String> mappedPredicates = new HashSet<String>();
		if (args.length == 3)
			if (args[2].endsWith("txt"))
				mappedPredicates = KyrieTerminal.getMappedPredicatesFromTXT(args[2]);
			else
				mappedPredicates = KyrieTerminal.getMappedPredicatesFromR2O(args[2]);

		String queryFile = args[0];
		String ontologyFile = args[1];
		DnD2DLexer lexer = new DnD2DLexer(new ANTLRFileStream(queryFile));
		DnD2DParser parser = new DnD2DParser(new CommonTokenStream(lexer));
		ArrayList<Clause> query = parser.program();
		ArrayList<Clause> original = KyrieTerminal.m_parser.getClauses(ontologyFile);

		Rewriter m_rewriter;
		if (args.length < 3)
			m_rewriter = new Rewriter(original);
		else
			m_rewriter = new Rewriter(original, mappedPredicates);
		long begin = System.currentTimeMillis();
		ArrayList<Clause> rewriting = m_rewriter.rewrite(query);
		long end = System.currentTimeMillis();
		System.out.println("Time spent in the process: " + (end - begin) + "ms\nClauses: " + rewriting.size());
	}
}
