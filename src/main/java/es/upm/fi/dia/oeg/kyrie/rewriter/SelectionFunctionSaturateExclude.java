// Copyright 2008 by Oxford University; see license.txt for details

package es.upm.fi.dia.oeg.kyrie.rewriter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


public class SelectionFunctionSaturateExclude extends SelectionFunction {

	private Set<String> excluded;

	public SelectionFunctionSaturateExclude(Collection<String> excluded) {
		this.excluded = new HashSet<String>(excluded);
	}

	@Override
	public boolean isToBePruned(Clause c) {
		return false;
	}

	@Override
	public void selectAtoms(Clause c) {

		// Initialize
		c.m_selectedHead = !this.excluded.contains(c.getHead().getName());
		c.m_selectedBody = new boolean[c.getBody().length];
		Term[] body = c.getBody();
		for (int i = 0; i < body.length; i++)
			c.m_selectedBody[i] = !this.excluded.contains(body[i].getName());
	}
}
