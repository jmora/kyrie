## kyrie

kyrie is a query rewriting system that started as a fork of [REQUIEM](http://www.cs.ox.ac.uk/isg/tools/Requiem/).
The supported expressiveness remains at $\mathcal{ELHIO}$ while the efficiency has been improved, as can be seen in the [evaluation](http://j.mp/benchmarkqueryrewiting).
If you want to cite this work you can find the corresponding BibTeX and other formats [here](http://www.bibsonomy.org/bibtex/27ea96e2fa828b7beae81a1388e6cf5aa/jmora).
There is also some benchmarking and evaluation addressed at this kind of systems that you can find as an [evaluation paper](http://www.bibsonomy.org/bibtex/27849812bcee0320ef55740fac72fe1b5/jmora).
The full documentation of the system for the internal and detailed aspects can be best found as my [thesis](http://j.mp/jmorathesis).
To run this system you don't need to know those internals, you have to options:

* The simple option is cloning this repository and using maven to generate a jar.
* The simplest option is to find that jar already available somewhere else (e.g. [v0.15.8](https://dl.dropboxusercontent.com/u/452942/maven/es/upm/fi/oeg/morph/kyrie/0.15.8))

Once you have the jar there are two ways to use it.

* As a [stand alone application](<kyrie/src/master/src/main/java/es/upm/fi/dia/oeg/kyrie/KyrieTerminal.java>), with only three arguments:
    - the query file, in a not picky Datalog, constants go between quotes.
    - the ontology file, in OWL, but what is beyond ELHIO will be dropped.
    - [optionally] you can add a mappings file, in R2O or plain text (so that's plain text for you). Each one of the mapped predicates should be in one line exactly as it appears in the ontology (but not the full URI).
* As a [library](<kyrie/src/master/src/main/java/es/upm/fi/dia/oeg/kyrie/KyrieLib.java>).
The methods in this case are analogous but they require a little bit of knowledge about the internal structures that are being used.
These methods should not change, but they may change.
Feel free to surf the code and please ask me for any question you may have.
    
It's possible and simple to configure kyrie to use full URIs or only partial URIs, but that may be done in a different way today, so ask for that.

For any comment, question or suggestion feel free to open an issue in the repository or to drop me an e-mail at live.com prepending my username in this site.